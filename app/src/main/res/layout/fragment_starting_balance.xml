<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">
    <data>
        <variable
            name="viewModel"
            type="com.bennorcombe.wyatt.ui.fragments.balances.viewmodels.BalanceHistoryViewModel" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        tools:context=".ui.fragments.balances.viewmodels.BalanceHistoryViewModel">

        <TextView
            android:id="@+id/first_balance_description"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_margin="16dp"
            android:textSize="18sp"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            android:text="@string/starting_balance_description" />
        <RadioGroup
            android:id="@+id/today_previous_balance_radio_group"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="horizontal"
            android:layout_margin="8dp"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/first_balance_description">
            <RadioButton android:id="@+id/today_radio"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:checked="true"
                android:text="@string/today_radio_label"/>
            <RadioButton android:id="@+id/previous_date_radio"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="@string/previous_date_radio_label"/>
        </RadioGroup>
        <androidx.constraintlayout.widget.ConstraintLayout
            android:id="@+id/previous_balance_layout"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:visibility="gone"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/today_previous_balance_radio_group">

            <TextView
                android:id="@+id/previous_balance_info_text"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_margin="16dp"
                android:textSize="16sp"
                android:text="@string/previous_balance_info"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent" />
            <androidx.appcompat.widget.AppCompatSpinner
                android:id="@+id/day_spinner"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginStart="8dp"
                android:layout_marginTop="8dp"
                android:layout_marginEnd="8dp"
                android:entries="@{viewModel.days}"
                android:selection="@{viewModel.daysPosition}"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/previous_balance_info_text" />

            <androidx.appcompat.widget.AppCompatSpinner
                android:id="@+id/month_spinner"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginStart="8dp"
                android:layout_marginTop="8dp"
                android:layout_marginEnd="8dp"
                android:entries="@{viewModel.months}"
                android:selection="@={viewModel.monthsPosition}"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/day_spinner" />

            <androidx.appcompat.widget.AppCompatSpinner
                android:id="@+id/year_spinner"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginStart="8dp"
                android:layout_marginTop="8dp"
                android:layout_marginEnd="8dp"
                android:entries="@{viewModel.years}"
                android:selection="@={viewModel.yearsPosition}"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/month_spinner" />

            <EditText
                android:id="@+id/previous_balance_amount"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_margin="12dp"
                android:ems="10"
                android:hint="@string/amount_hint"
                android:inputType="numberDecimal"
                android:text="@={viewModel.previousDateAmount}"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/year_spinner"
                android:autofillHints="Balance amount for a previous date" />

        </androidx.constraintlayout.widget.ConstraintLayout>

        <TextView
            android:id="@+id/balance_amount_text"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="@string/today_balance_label"
            android:layout_margin="12dp"
            android:textSize="16sp"
            android:visibility="gone"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/previous_balance_layout">
        </TextView>

        <EditText
            android:id="@+id/balance_amount"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_margin="12dp"
            android:ems="10"
            android:hint="@string/amount_hint"
            android:inputType="numberDecimal"
            android:text="@={viewModel.amount}"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/balance_amount_text"
            android:autofillHints="Your balance right now" />

        <com.google.android.material.floatingactionbutton.FloatingActionButton
            android:id="@+id/fab_save_start_balance"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_gravity="bottom|end"
            android:layout_margin="@dimen/fab_margin"
            android:layout_marginEnd="8dp"
            android:layout_marginBottom="8dp"
            android:src="@drawable/ic_navigate_next_white_24"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent" />
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>