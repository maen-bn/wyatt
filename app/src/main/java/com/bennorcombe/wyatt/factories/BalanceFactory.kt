package com.bennorcombe.wyatt.factories

import android.content.Context
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.services.AndroidNotificationService
import com.bennorcombe.wyatt.services.BalanceService

class BalanceFactory {
    fun makeService(context: Context): BalanceService {
        return BalanceService(
            AppDatabase.getInstance(context).balanceDao(),
            BankFactory().makeService(context),
            AndroidNotificationService(context)
        )
    }
}