package com.bennorcombe.wyatt.factories

import com.bennorcombe.wyatt.BuildConfig
import com.plaid.linkbase.models.configuration.LinkConfiguration
import com.plaid.linkbase.models.configuration.PlaidEnvironment
import com.plaid.linkbase.models.configuration.PlaidProduct
import com.plaid.linkbase.models.connection.PlaidLinkResultHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class OAuthProviderFactory {

    private val coroutineScope = CoroutineScope(Dispatchers.IO)

    fun makeLinkConfiguration(appName: String, token: String? = null): LinkConfiguration {
        return LinkConfiguration(
            clientName = appName,
            token = token,
            environment = PlaidEnvironment.valueOf(BuildConfig.PLAID_ENVIRONMENT),
            products = listOf(PlaidProduct.TRANSACTIONS),
            countryCodes = listOf(Locale.UK.country)
        )
    }

    fun makeResultHandler(success: (publicToken: String, bankName: String) -> Unit, cancelled: () -> Unit): PlaidLinkResultHandler {
        return PlaidLinkResultHandler(
            requestCode = LINK_REQUEST_CODE,
            onSuccess = {
                coroutineScope.launch {
                    val bankName = it.linkConnectionMetadata.institutionName?:"Not Found"
                    success(it.publicToken, bankName)
                }
            },
            onCancelled = {
                cancelled()
            }
        )
    }

    companion object {
        const val LINK_REQUEST_CODE = 1
    }
}