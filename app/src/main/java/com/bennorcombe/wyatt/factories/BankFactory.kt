package com.bennorcombe.wyatt.factories

import android.content.Context
import androidx.preference.PreferenceManager
import androidx.work.WorkManager
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.services.*
import com.bennorcombe.wyatt.workers.AndroidScheduleBalanceUpdate
import com.bennorcombe.wyatt.workers.ScheduleBalanceUpdate
import okhttp3.OkHttpClient

class BankFactory {
    fun makeService(context: Context): BankService {
        val oAuthService = PlaidOAuthService(
            KeyStoreService(),
            PreferenceManager.getDefaultSharedPreferences(context),
            OAuthProviderFactory()
        )

        return PlaidBankService(
            oAuthService,
            PlaidApiService(OkHttpClient()),
            AppDatabase.getInstance(context).bankDao(),
            makeUpdateBalanceScheduler(context)
        )
    }

    private fun makeUpdateBalanceScheduler(context: Context): ScheduleBalanceUpdate {
        return AndroidScheduleBalanceUpdate(WorkManager.getInstance(context))
    }
}