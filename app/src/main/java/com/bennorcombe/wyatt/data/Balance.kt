package com.bennorcombe.wyatt.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import org.joda.time.DateTime
import java.math.BigDecimal

@Entity(tableName = "balances", indices = [Index(value = ["date"], unique = true)])
data class Balance(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo(name = "amount") var amount: BigDecimal,
    @ColumnInfo(name = "current") var current: Boolean = true,
    @ColumnInfo(name = "date") var date: DateTime?
    )