package com.bennorcombe.wyatt.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface OutgoingDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(outgoing: Outgoing)

    @Transaction
    @Query("SELECT * FROM outgoings")
    fun getOutgoingsAndType(): LiveData<List<OutgoingAndOutgoingType>>

    @Query("SELECT * FROM outgoings WHERE id = :id")
    fun find(id: Int): LiveData<Outgoing>
}