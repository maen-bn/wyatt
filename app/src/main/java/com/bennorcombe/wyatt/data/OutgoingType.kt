package com.bennorcombe.wyatt.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "outgoing_types")
data class OutgoingType(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "name") val name: String
) {
    override fun toString(): String {
        return name
    }
}
