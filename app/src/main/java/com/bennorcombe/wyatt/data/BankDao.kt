package com.bennorcombe.wyatt.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface BankDao {
    @Query("SELECT * FROM banks")
    fun all(): LiveData<List<Bank>>

    @Query("SELECT * FROM banks")
    fun allNow(): List<Bank>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(bank: Bank)

    @Query("SELECT * FROM banks WHERE id = :id")
    fun find(id: Int): LiveData<Bank>

    @Query("SELECT * FROM banks WHERE id = :id")
    fun findNow(id: Int): Bank

    @Query("SELECT * FROM banks WHERE external_id = :externalId")
    fun findByExternalId(externalId: String): Bank
}