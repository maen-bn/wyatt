package com.bennorcombe.wyatt.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface OutgoingsTypeDao {
    @Query("SELECT * FROM outgoing_types")
    fun all(): List<OutgoingType>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(outgoingType: List<OutgoingType>)
}