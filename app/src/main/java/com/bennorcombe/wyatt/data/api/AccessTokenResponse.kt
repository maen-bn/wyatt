package com.bennorcombe.wyatt.data.api

import org.json.JSONObject

data class AccessTokenResponse(val token: String, val body: JSONObject)