package com.bennorcombe.wyatt.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import org.joda.time.DateTime

@Dao
interface BalanceDao {
    @Query("SELECT * FROM balances WHERE id = :id")
    fun find(id: Int): LiveData<Balance>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(balance: Balance)

    @Query("SELECT * FROM balances WHERE current = 1 AND date IS NULL LIMIT 1")
    fun current(): LiveData<Balance>

    @Query("SELECT * FROM balances WHERE current = 1 AND date IS NULL LIMIT 1")
    fun currentNow(): Balance

    @Query("SELECT * FROM balances WHERE current = 0 ORDER BY date DESC")
    fun history(): LiveData<List<Balance>>

    @Query("SELECT * FROM balances ORDER BY current DESC, date DESC LIMIT 2;")
    fun currentAndPreviousMonth(): LiveData<List<Balance>>

    @Query("SELECT * FROM balances WHERE (date >= :startDate AND date < :endDate) OR current = 1 ORDER BY current DESC, date DESC;")
    fun allByRange(startDate: DateTime, endDate: DateTime): LiveData<List<Balance>>
}