package com.bennorcombe.wyatt.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.joda.time.DateTime
import java.math.BigDecimal

@Entity(tableName = "banks")
data class Bank(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo(name = "external_id") var externalId: String? = null,
    @ColumnInfo(name = "auth_update_required") var authUpdateRequired: Boolean = false,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "manual_balance") var manualBalance: BigDecimal? = null,
    @ColumnInfo(name = "estimated_change") var estimatedChange: BigDecimal? = null,
    @ColumnInfo(name = "change_day") var changeDay: Int? = null,
    @ColumnInfo(name = "estimated_change_last_date") var estimatedChangeLastDate: DateTime? = null
) {
    fun isManualBank(): Boolean {
        val hasEstChange = estimatedChange != null && estimatedChangeLastDate != null
        return externalId == null && hasEstChange && manualBalance != null
    }

    fun isManualUpdateRequired(): Boolean {
        val day = DateTime.now().dayOfMonth().asString.toInt()
        val month = DateTime.now().monthOfYear().asString.toInt()
        val changeDay = calculateChangeDay(changeDay?:1, month)
        val lastChangedMonth = estimatedChangeLastDate?.monthOfYear()?.asString?.toInt()
        val withinDayRange = day >= changeDay && day < day()

        return withinDayRange && month != lastChangedMonth
    }

    private fun calculateChangeDay(originalChangeDay: Int, month: Int): Int {
        var changeDay = originalChangeDay
        val dayOfWeek = DateTime.parse("$currentYear-$month-$originalChangeDay")
            .dayOfWeek().asString.toInt()
        if (dayOfWeek == 6 || dayOfWeek == 7) {
            changeDay += (8 - dayOfWeek)
        }
        return changeDay
    }
}