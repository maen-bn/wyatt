package com.bennorcombe.wyatt.data

import androidx.room.TypeConverter
import org.joda.time.DateTime
import java.math.BigDecimal

class Converters {
    @TypeConverter
    fun fromInt(value: Int?): BigDecimal? {
        return value?.toBigDecimal()?.divide(BigDecimal(100))
    }

    @TypeConverter
    fun bigDecimalToInt(value: BigDecimal?): Int? {
        return value?.multiply(BigDecimal(100))?.toInt()
    }

    @TypeConverter
    fun fromString(value: String?): DateTime? {
        var date: DateTime? = null
        if (value != null) {
            date = DateTime(value)
        }
        return date
    }

    @TypeConverter
    fun dateToString(value: DateTime?): String? {
        return value?.toString()
    }

}