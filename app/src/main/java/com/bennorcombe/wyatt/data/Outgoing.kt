package com.bennorcombe.wyatt.data

import androidx.room.*
import java.math.BigDecimal

@Entity(tableName = "outgoings")
data class Outgoing(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "amount") var amount: BigDecimal,
    @ColumnInfo(name = "outgoing_type_id") var outgoingTypeId: Int,
    @ColumnInfo(name = "occurrence") var occurrence: Int? = null
)

data class OutgoingAndOutgoingType(
    @Embedded val outgoing: Outgoing,
    @Relation(
        parentColumn = "outgoing_type_id",
        entityColumn = "id"
    )
    val type: OutgoingType
)
