package com.bennorcombe.wyatt.data

import org.joda.time.DateTime
import java.util.*

val currentYear = Calendar.getInstance().get(Calendar.YEAR)

fun day(): Int {
    return 23
}

fun days(year: Int, month: Int): List<Int> {
    val date: DateTime = DateTime.parse("$year-$month-01")
    val max = date.dayOfMonth().maximumValue
    return listOf(1..max).flatten()
}

fun startDate(): DateTime {
    val current = DateTime.now()
    val currentMonth = current.toString("M").toInt()
    var year = currentYear
    if(currentMonth < 7) {
        year -= 1
    }
    return DateTime.parse("$year-07-${day()}")
}

fun endDate(): DateTime {
    return startDate().minusDays(1).plusYears(1)
}

fun months():List<String> {
    return listOf(
        "January", "February", "March", "April", "May", "June",
         "July", "August", "September", "October", "November",
         "December"
    )
}

fun years(): List<Int> {
    val startYear = currentYear - 2
    val yearRange = (startYear..currentYear)
    val years = mutableListOf<Int>()
    for (y in yearRange){
        years.add(y)
    }

    return years.toList()
}