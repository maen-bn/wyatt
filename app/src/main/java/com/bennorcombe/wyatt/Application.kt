package com.bennorcombe.wyatt

import android.app.Application
import androidx.work.Configuration
import androidx.work.DelegatingWorkerFactory
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.workers.WorkerFactory
import com.plaid.link.Plaid
import com.plaid.linkbase.models.configuration.PlaidEnvironment
import com.plaid.linkbase.models.configuration.PlaidOptions
import com.plaid.log.LogLevel
import net.danlew.android.joda.JodaTimeAndroid

class Application: Application(), Configuration.Provider{
    override fun onCreate() {
        super.onCreate()
        AppDatabase.getInstance(this)
        JodaTimeAndroid.init(this)
        plaidSetup()
    }

    private fun plaidSetup() {
        val plaidOptions = PlaidOptions(
            if (BuildConfig.DEBUG) LogLevel.VERBOSE else LogLevel.ASSERT,
            PlaidEnvironment.SANDBOX
        )
        Plaid.setOptions(plaidOptions)
        Plaid.setPublicKey(BuildConfig.PLAID_API_PUBLIC_KEY)
    }

    override fun getWorkManagerConfiguration(): Configuration {
        val delegatingWorkerFactory = DelegatingWorkerFactory()
        delegatingWorkerFactory.addFactory(WorkerFactory())
        return Configuration.Builder()
            .setWorkerFactory(delegatingWorkerFactory)
            .setMinimumLoggingLevel(android.util.Log.INFO)
            .build()
    }
}