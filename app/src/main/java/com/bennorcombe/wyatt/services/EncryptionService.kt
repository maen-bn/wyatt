package com.bennorcombe.wyatt.services

interface EncryptionService {

    fun encrypt(alias: String, toEncrypt: String): String

    fun iv(): String

    fun decrypt(alias: String, encrypted: String, iv: String): String?
}