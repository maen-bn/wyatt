package com.bennorcombe.wyatt.services

import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import androidx.annotation.NonNull
import java.io.IOException
import java.security.*
import javax.crypto.*
import javax.crypto.spec.GCMParameterSpec

class KeyStoreService: EncryptionService {
    companion object{
        const val TRANSFORMATION = "AES/GCM/NoPadding"
        const val ANDROID_KEY_STORE = "AndroidKeyStore"
    }

    private lateinit var encryption: ByteArray
    private lateinit var iv: ByteArray
    private val keyStore = KeyStore.getInstance(ANDROID_KEY_STORE)
    private val cipher = Cipher.getInstance(TRANSFORMATION)
    private val keyGenerator = KeyGenerator.getInstance(
        KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEY_STORE
    )
    private val dataCharSet = Charsets.ISO_8859_1

    init {
        keyStore.load(null)
    }

    @Throws(
        UnrecoverableEntryException::class,
        NoSuchAlgorithmException::class,
        KeyStoreException::class,
        NoSuchProviderException::class,
        NoSuchPaddingException::class,
        InvalidKeyException::class,
        IOException::class,
        InvalidAlgorithmParameterException::class,
        SignatureException::class,
        BadPaddingException::class,
        IllegalBlockSizeException::class
    )
    override fun encrypt(alias: String, toEncrypt: String): String {
        cipher.init(Cipher.ENCRYPT_MODE, getSecretKey(alias))
        iv = cipher.iv
        return cipher.doFinal(toEncrypt.toByteArray(charset("UTF-8")))
            .also { encryption = it }.toString(dataCharSet)
    }

    override fun iv(): String {
        return iv.toString(dataCharSet)
    }

    @Throws(
        UnrecoverableEntryException::class,
        NoSuchAlgorithmException::class,
        KeyStoreException::class,
        NoSuchProviderException::class,
        NoSuchPaddingException::class,
        InvalidKeyException::class,
        IOException::class,
        BadPaddingException::class,
        IllegalBlockSizeException::class,
        InvalidAlgorithmParameterException::class
    )
    override fun decrypt(alias: String, encrypted: String, iv: String): String? {
        val spec = GCMParameterSpec(128, iv.toByteArray(dataCharSet))
        cipher.init(Cipher.DECRYPT_MODE, getSecretKey(alias), spec)
        return cipher.doFinal(encrypted.toByteArray(dataCharSet)).toString(charset("UTF-8"))
    }

    @NonNull
    @Throws(
        NoSuchAlgorithmException::class,
        NoSuchProviderException::class,
        InvalidAlgorithmParameterException::class
    )
    private fun getSecretKey(alias: String): SecretKey? {
        var secretKey = (keyStore?.getEntry(alias, null) as KeyStore.SecretKeyEntry?)?.secretKey

        if (secretKey == null) {
            keyGenerator.init(
                KeyGenParameterSpec.Builder(
                    alias,
                    KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
                )
                    .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                    .build()
            )
            secretKey =  keyGenerator.generateKey()
        }

        return secretKey
    }
}