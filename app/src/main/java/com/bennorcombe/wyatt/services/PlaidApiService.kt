package com.bennorcombe.wyatt.services

import com.bennorcombe.wyatt.BuildConfig
import com.bennorcombe.wyatt.data.api.AccessTokenResponse
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.io.IOException

interface BankApiService {
    fun accessToken(publicToken: String, handleResponse: (AccessTokenResponse) -> Unit)

    fun publicToken(accessToken: String, onResponse: (JSONObject) -> Unit)

    fun balance(accessToken: String): JSONObject
}

class PlaidApiService(
    private val client: Call.Factory,
    baseApiUrl: String = BuildConfig.PLAID_API_URL): BankApiService {

    private val accessTokenUrl = "$baseApiUrl/item/public_token/exchange"

    private val publicTokenUrl = "$baseApiUrl/item/public_token/create"

    private val balanceUrl = "$baseApiUrl/accounts/balance/get"

    private val mediaType = "application/json; charset=utf-8".toMediaTypeOrNull()

    override fun accessToken(publicToken: String, handleResponse: (AccessTokenResponse) -> Unit) {
        val request = request(accessTokenUrl, requestBody("public_token", publicToken))

        call(request){
            val a = AccessTokenResponse(it.get("access_token").toString(), it)
            handleResponse(a)
        }
    }

    override fun publicToken(accessToken: String, onResponse: (JSONObject) -> Unit) {
        val request = request(publicTokenUrl, requestBody("access_token", accessToken))

        call(request) {
            onResponse(it)
        }
    }

    override fun balance(accessToken: String): JSONObject {
        val request = request(balanceUrl, requestBody("access_token", accessToken))
        val response = client.newCall(request).execute()
        return JSONObject(response.body?.string().toString())
    }

    private fun request(url: String, body: RequestBody): Request {
        return Request.Builder()
            .header("Content-Type", "application/json")
            .url(url)
            .post(body)
            .build()
    }

    private fun requestBody(tokenKey: String, token: String): RequestBody {
        val json = JSONObject()
        json.put(tokenKey, token)
        json.put("client_id", BuildConfig.PLAID_API_CLIENT_ID)
        json.put("secret", BuildConfig.PLAID_API_SECRET)

        return json.toString().toRequestBody(mediaType)
    }

    private fun call(request: Request, onResponse: (JSONObject) -> Unit) {
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {}
            override fun onResponse(call: Call, response: Response) {
                val jsonResponse = JSONObject(response.body?.string().toString())
                onResponse(jsonResponse)
            }
        })
    }
}