package com.bennorcombe.wyatt.services

import android.content.Intent
import android.content.SharedPreferences
import androidx.fragment.app.Fragment
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.factories.OAuthProviderFactory
import com.plaid.link.Plaid
import com.plaid.linkbase.models.connection.PlaidLinkResultHandler

interface BankOAuthService {
    var publicToken: String?

    fun auth(fragment: Fragment, onSuccess: (publicToken: String, name: String) -> Unit, onCancelled: () -> Unit = {}): Boolean

    fun onResult(requestCode: Int, resultCode: Int, data: Intent?)

    fun accessToken(itemId: String): String?

    fun saveAccessToken(accessToken: String, externalId: String)
}

class PlaidOAuthService(
    private val encryptionService: EncryptionService,
    private val sharedPreferences: SharedPreferences,
    private val oAuthProviderFactory: OAuthProviderFactory): BankOAuthService {

    override var publicToken: String? = null

    private lateinit var resultHandler: PlaidLinkResultHandler

    override fun auth(fragment: Fragment, onSuccess: (publicToken: String, name: String) -> Unit, onCancelled: () -> Unit): Boolean {
        resultHandler = oAuthProviderFactory.makeResultHandler(onSuccess, onCancelled)
        val appName = fragment.getString(R.string.app_name)
        val token = publicToken
        publicToken = null
        return Plaid.openLink(
            fragment = fragment,
            linkConfiguration = oAuthProviderFactory.makeLinkConfiguration(appName, token),
            requestCode = OAuthProviderFactory.LINK_REQUEST_CODE
        )
    }

    override fun onResult(requestCode: Int, resultCode: Int, data: Intent?) {
        resultHandler.onActivityResult(requestCode, resultCode, data)
    }

    override fun accessToken(itemId: String): String? {
        val encryptedToken = sharedPreferences.getString("$itemId${ENCRYPTED_TOKEN_KEY}", null)
        val iv = sharedPreferences.getString("$itemId${IV_KEY}", null)
        return encryptionService.decrypt("$itemId${TOKEN_KEY}", encryptedToken!!, iv!!)
    }

    override fun saveAccessToken(accessToken: String, externalId: String) {
        val e = encryptionService.encrypt("$externalId${TOKEN_KEY}", accessToken)
        val editor = sharedPreferences.edit()
        editor?.putString("$externalId${IV_KEY}", encryptionService.iv())
        editor?.putString("$externalId${ENCRYPTED_TOKEN_KEY}", e)
        editor?.apply()
    }

    companion object {
        private const val ENCRYPTED_TOKEN_KEY = "-ENCRYPTED-ACCESS-TOKEN"
        private const val TOKEN_KEY = "-ACCESS-TOKEN"
        private const val IV_KEY = "-IV"
    }
}