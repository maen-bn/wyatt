package com.bennorcombe.wyatt.services

import com.bennorcombe.wyatt.data.OutgoingAndOutgoingType
import java.math.BigDecimal
import java.math.RoundingMode

class BudgetService(val outgoings: List<OutgoingAndOutgoingType>) {
    fun monthlyLow(): BigDecimal {
        var amount = BigDecimal(0)
        outgoings.filter { it.type.id == 1 }.map { it.outgoing.amount }.forEach {
            amount += it
        }

        return amount.setScale(2, RoundingMode.HALF_EVEN)
    }

    fun monthlyHigh(): BigDecimal {
        var amount = BigDecimal(0)
        outgoings.map {
            var a = it.outgoing.amount
            if (it.type.id == 1){
                a = it.outgoing.amount.multiply(BigDecimal(it.outgoing.occurrence?:12))
            }
            a
        }.forEach {
            amount += it
        }

        return amount.setScale(2, RoundingMode.HALF_EVEN) / BigDecimal(12)
    }

    fun monthlyMedian(): BigDecimal = (monthlyLow() + monthlyHigh())
        .divide(BigDecimal(2))
        .setScale(2, RoundingMode.HALF_EVEN)
}