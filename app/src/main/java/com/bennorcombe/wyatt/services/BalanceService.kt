package com.bennorcombe.wyatt.services

import com.bennorcombe.wyatt.data.BalanceDao
import com.bennorcombe.wyatt.data.Bank
import java.math.BigDecimal

class BalanceService(
    private val balanceDao: BalanceDao,
    private val bankService: BankService,
    private val notificationService: NotificationService) {

    fun update() {
        try {
            val balance = balanceDao.currentNow()
            balance.amount = bankService.totalBankBalance()
            balanceDao.save(balance)
        } catch (e: BankBalanceException) {
            notificationService.notifyBankAuthUpdate(e.bank)
        }
    }
}