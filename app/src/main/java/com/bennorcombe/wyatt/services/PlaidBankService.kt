package com.bennorcombe.wyatt.services

import android.content.Intent
import androidx.fragment.app.Fragment
import com.bennorcombe.wyatt.data.Bank
import com.bennorcombe.wyatt.data.BankDao
import com.bennorcombe.wyatt.workers.ScheduleBalanceUpdate
import org.json.JSONArray
import org.json.JSONObject
import java.math.BigDecimal

interface BankService {
    fun add(fragment: Fragment, onCancelled: () -> Unit = {}): Boolean

    fun update(fragment: Fragment, bank: Bank): Boolean

    fun onAddResult(requestCode: Int, resultCode: Int, data: Intent?)

    fun updateBalances()

    fun totalBankBalance(): BigDecimal
}

class PlaidBankService(
    private val oAuthService: BankOAuthService,
    private val apiService: BankApiService,
    private val bankDao: BankDao,
    private val scheduleBalanceUpdate: ScheduleBalanceUpdate) : BankService {

    override fun add(fragment: Fragment, onCancelled: () -> Unit): Boolean {
        oAuthService.auth(fragment, this::handleSuccessfulAuth, onCancelled)
        return true
    }

    override fun update(fragment: Fragment, bank: Bank): Boolean {
        val externalId = bank.externalId
        externalId ?: return false

        val accessToken = oAuthService.accessToken(externalId)
        accessToken ?: return false

        apiService.publicToken(accessToken) {
            oAuthService.publicToken = it.get("public_token").toString()
            oAuthService.auth(fragment, onSuccess = {_, _ ->
                val b = bank.copy(authUpdateRequired = false)
                bankDao.save(b)
            })
        }

        return true
    }

    override fun onAddResult(requestCode: Int, resultCode: Int, data: Intent?) {
        oAuthService.onResult(requestCode, resultCode, data)
    }

    override fun updateBalances() {
        scheduleBalanceUpdate.schedule()
    }

    override fun totalBankBalance(): BigDecimal {
        var newTotalBalance = BigDecimal(0)
        bankDao.allNow().forEach { bank ->
            newTotalBalance += when(bank.isManualBank()){
                false -> (balance(bank))
                true -> updatedManualBalance(bank)
            }
        }
        return newTotalBalance
    }

    private fun handleSuccessfulAuth(publicToken: String, bankName: String) {
        apiService.accessToken(publicToken) {
            val externalId = it.body.get("item_id").toString()
            oAuthService.saveAccessToken(it.token, externalId)
            saveAndUpdateBalances(externalId, bankName)
        }
    }

    private fun saveAndUpdateBalances(externalId: String, name: String)  {
        val bank = Bank(externalId = externalId, name = name)
        bankDao.save(bank)
        updateBalances()
    }

    private fun balance(bank: Bank): BigDecimal {
        return oAuthService.accessToken(bank.externalId!!)?.let {accessToken ->
            val json = apiService.balance(accessToken)
            if(json.has("error_code")) {
                bankDao.save(bank.copy(authUpdateRequired = true))
                throw BankBalanceException(bank)
            }
            calculateBalance(json)
        }?: throw BankBalanceException(bank)
    }

    private fun calculateBalance(json: JSONObject): BigDecimal {
        val accounts = json.get("accounts") as JSONArray
        var balance = BigDecimal(0)
        for (i in 0 until accounts.length()) {
            val balances = (accounts.get(i) as JSONObject).get("balances") as JSONObject
            var b = balances.get("available")
            if (b.toString() == "null") {
                b = balances.get("current")
            }
            balance += BigDecimal(b.toString())
        }
        return balance
    }

    private fun updatedManualBalance(bank: Bank): BigDecimal {
        val change = bank.estimatedChange!!
        val manualBalance = bank.manualBalance!!
        if(bank.isManualUpdateRequired()){
            bank.manualBalance = manualBalance.plus(change)
            bankDao.save(bank)
        }
        return bank.manualBalance?: BigDecimal(0)
    }
}

class BankBalanceException(val bank: Bank): Exception()