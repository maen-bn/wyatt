package com.bennorcombe.wyatt.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.navigation.NavDeepLinkBuilder
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.data.Bank
import com.bennorcombe.wyatt.ui.activities.MainActivity

interface NotificationService {
    fun notifyBankAuthUpdate(bank: Bank)
}

class AndroidNotificationService(private val context: Context): NotificationService {

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Wyatt General Channel"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel = NotificationChannel(CHANNEL_ID, name, importance)
            mChannel.description = ""
            val manager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(mChannel)
        }
    }

    override fun notifyBankAuthUpdate(bank: Bank) {
        val builder = builder(
            context.getString(R.string.notification_bank_re_auth_title),
            context.getString(R.string.notification_bank_re_auth_text, bank.name),
            R.id.nav_banks
        )

        with(NotificationManagerCompat.from(context)) {
            notify(43, builder.build())
        }
    }

    private fun builder(title: String, text: String, destination: Int): NotificationCompat.Builder {
        return NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(title)
            .setContentText(text)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent(destination))
            .setAutoCancel(true)
    }

    private fun pendingIntent(destination: Int): PendingIntent {
        return NavDeepLinkBuilder(context)
            .setComponentName(MainActivity::class.java)
            .setGraph(R.navigation.mobile_navigation)
            .setDestination(destination)
            .createPendingIntent()
    }

    companion object {
        private const val CHANNEL_ID = "wyatt"
    }
}