package com.bennorcombe.wyatt.services

import com.bennorcombe.wyatt.data.Balance
import com.bennorcombe.wyatt.data.months
import org.joda.time.DateTime
import java.math.BigDecimal
import java.math.RoundingMode

class OverviewService(private val budgetService: BudgetService) {

    fun month(balance: Balance, previous: Balance): List<BigDecimal> {
        val spent = previous.amount - balance.amount
        val low =  budgetService.monthlyLow().subtract(spent)
        val median = budgetService.monthlyMedian().subtract(spent)
        val high = budgetService.monthlyHigh().subtract(spent)

        return listOf(low,median,high)
    }

    fun all(balances: List<Balance>): List<BigDecimal>{
        val overview = mutableListOf(BigDecimal(0), BigDecimal(0), BigDecimal(0))
        balances.forEachIndexed { index, balance ->
            val nextBalance = balances.getOrNull(index + 1)
            if(nextBalance != null) {
                val o = month(balance, nextBalance)
                overview[0] += o[0]
                overview[1] += o[1]
                overview[2] += o[2]
            }
        }

        return overview.toList()
    }

    fun totalGainLoss(balances: List<Balance>): BigDecimal {
        var total = BigDecimal(0.00)
        balances.forEachIndexed { index, balance ->
            val nextBalance = balances.getOrNull(index + 1)
            total = total.add(nextBalance?.amount?.subtract(balance.amount)?:BigDecimal(0.00))
        }
        return total.setScale(2, RoundingMode.HALF_EVEN)
    }

    fun averageGainLoss(balances: List<Balance>): BigDecimal {
        val b = balances.filter { !it.current }
        var count = BigDecimal(b.count() - 1)
        if(count == BigDecimal(0)){
            count = BigDecimal(1)
        }
        return totalGainLoss(b) / count
    }

    fun predictedSpend(balances: List<Balance>, months: Int = 12): BigDecimal {
        return averageGainLoss(balances).multiply(BigDecimal(months))
    }

    fun balanceChangeHistory(balances: List<Balance>) : Map<String, BigDecimal> {
        val changeHistory = mutableMapOf<String, BigDecimal>()
        balances.reversed().filter { !it.current }.forEachIndexed { index, balance ->
            val previousBalance = balances.reversed().getOrNull(index - 1)
            if (previousBalance != null) {
                val change = balance.amount.subtract(previousBalance.amount)
                val monthOfYear = balance.date!!.monthOfYear
                missingMonths(balance.date!!, previousBalance.date!!).forEach {
                    changeHistory[months()[it - 1]] = change
                }
                changeHistory[months()[monthOfYear - 1]] = change
            }
        }
        return changeHistory
    }

    private fun missingMonths(balanceDate: DateTime, previousBalanceDate: DateTime): List<Int> {
        var monthOfYear = balanceDate.monthOfYear

        if (balanceDate.year > previousBalanceDate.year) {
            monthOfYear += 12
        }
        return ((previousBalanceDate.monthOfYear + 1) until monthOfYear).toList().map {
            var month = it
            if (month > 12) month -= 12
            month
        }
    }
}