package com.bennorcombe.wyatt.workers

import android.content.Context
import androidx.preference.PreferenceManager
import androidx.work.ListenableWorker
import androidx.work.WorkerParameters
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.factories.BalanceFactory
import com.bennorcombe.wyatt.factories.BankFactory
import com.bennorcombe.wyatt.services.AndroidNotificationService
import com.bennorcombe.wyatt.services.BalanceService
import androidx.work.WorkerFactory as BaseWorkerFactory

class WorkerFactory : BaseWorkerFactory() {
    private val balanceFactory = BalanceFactory()

    override fun createWorker(
        appContext: Context,
        workerClassName: String,
        workerParameters: WorkerParameters
    ): ListenableWorker? {
        return when(workerClassName) {
            SeedDatabaseWorker::class.java.name ->
                SeedDatabaseWorker(appContext, workerParameters)
            UpdateBalanceWorker::class.java.name ->
                UpdateBalanceWorker(
                    appContext,
                    workerParameters,
                    balanceFactory.makeService(appContext),
                    PreferenceManager.getDefaultSharedPreferences(appContext)
                )
            else -> null
        }
    }
}