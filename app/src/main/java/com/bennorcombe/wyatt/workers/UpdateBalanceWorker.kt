package com.bennorcombe.wyatt.workers

import android.content.Context
import android.content.SharedPreferences
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.bennorcombe.wyatt.services.BalanceService

class UpdateBalanceWorker(
    context: Context,
    workerParams: WorkerParameters,
    private val balanceService: BalanceService,
    private val preferences: SharedPreferences
) : Worker(context, workerParams) {

    override fun doWork(): Result {
        val autoBalanceOn = preferences.getBoolean("auto_balance_preference", false)
        if(autoBalanceOn) {
            balanceService.update()
        }
        return Result.success()
    }
}