package com.bennorcombe.wyatt.workers

import androidx.work.*
import com.bennorcombe.wyatt.services.PlaidBankService
import java.util.concurrent.TimeUnit

interface ScheduleBalanceUpdate {
    fun schedule()
}

class AndroidScheduleBalanceUpdate(private val workManager: WorkManager): ScheduleBalanceUpdate {
    override fun schedule() {

        if (workManager.getWorkInfosForUniqueWork(PERIODIC_BALANCE_UPDATE_TAG).get().size > 0) {
            workManager.enqueue(oneTimeWorkRequest())
        }
        workManager.enqueueUniquePeriodicWork(
            PERIODIC_BALANCE_UPDATE_TAG, ExistingPeriodicWorkPolicy.KEEP , periodicWorkRequest()
        )
    }

    private fun oneTimeWorkRequest(): WorkRequest = OneTimeWorkRequestBuilder<UpdateBalanceWorker>()
        .build()

    private fun periodicWorkRequest(): PeriodicWorkRequest =
        PeriodicWorkRequestBuilder<UpdateBalanceWorker>(12, TimeUnit.HOURS).build()

    companion object {
        private const val PERIODIC_BALANCE_UPDATE_TAG = "periodicBalanceUpdate"
    }
}