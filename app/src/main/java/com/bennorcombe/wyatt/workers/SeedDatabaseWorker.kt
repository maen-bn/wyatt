package com.bennorcombe.wyatt.workers

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.data.Balance
import com.bennorcombe.wyatt.data.OutgoingType
import com.bennorcombe.wyatt.utils.OUTGOINGS_TYPES_DATA_FILENAME
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import kotlinx.coroutines.coroutineScope
import java.math.BigDecimal

class SeedDatabaseWorker(
    context: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(context, workerParams) {
    override suspend fun doWork(): Result = coroutineScope {
        try {
            applicationContext.assets.open(OUTGOINGS_TYPES_DATA_FILENAME).use { inputStream ->
                JsonReader(inputStream.reader()).use { jsonReader ->
                    val outgoingsTypeType = object : TypeToken<List<OutgoingType>>() {}.type
                    val outgoingTypeList: List<OutgoingType> = Gson().fromJson(jsonReader, outgoingsTypeType)

                    val database = AppDatabase.getInstance(applicationContext)
                    database.outgoingsTypeDao().insertAll(outgoingTypeList)

                    Result.success()
                }
            }
        } catch (ex: Exception) {
            Log.e(TAG, "Error seeding database", ex)
            Result.failure()
        }
    }

    companion object {
        private val TAG = SeedDatabaseWorker::class.java.simpleName
    }
}