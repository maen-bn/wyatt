package com.bennorcombe.wyatt.ui.fragments.balances.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.bennorcombe.wyatt.data.*
import org.joda.time.DateTime

class BalanceHistoryViewModel(application: Application, balanceId: Int) : AndroidViewModel(application) {

    val balance = AppDatabase.getInstance(application).balanceDao().find(balanceId)

    var days = MutableLiveData<List<Int>>(listOf(1..28).flatten())

    val months= months()

    val years= years().reversed()

    val amount = MutableLiveData<String>("0")

    val previousDateAmount = MutableLiveData<String>("0")

    val daysPosition = MutableLiveData(0)

    var monthsPosition = MutableLiveData(0)

    var yearsPosition = MutableLiveData(0)

    val month
        get() =
            monthsPosition.value?.let {
                it + 1
            }
    val year
        get() =
            yearsPosition.value?.let {
                years[it]
            }

    val date: DateTime
        get() {
            return DateTime("$year-$month-${day()}")
        }
}
