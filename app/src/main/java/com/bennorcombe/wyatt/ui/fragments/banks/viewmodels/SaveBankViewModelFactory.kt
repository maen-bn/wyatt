package com.bennorcombe.wyatt.ui.fragments.banks.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class SaveBankViewModelFactory(private val bankId: Int): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SaveBankViewModel(
            bankId
        ) as T
    }
}