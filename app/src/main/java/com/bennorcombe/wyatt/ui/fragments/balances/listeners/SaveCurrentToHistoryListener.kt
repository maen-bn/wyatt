package com.bennorcombe.wyatt.ui.fragments.balances.listeners

import android.os.AsyncTask
import android.view.View
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.data.day
import com.bennorcombe.wyatt.ui.fragments.balances.viewmodels.BalancesViewModel
import org.joda.time.DateTime
import org.joda.time.LocalDate

class SaveCurrentToHistoryListener(private val viewModel: BalancesViewModel): View.OnClickListener {
    override fun onClick(view: View?) {
        AsyncTask.execute{
            val cal = LocalDate.now()
            val date = DateTime.parse(
                "${cal.toString("yyyy")}-${cal.toString("MM")}-${day()}"
            )
            val balance = viewModel.current.value?.copy(id = 0, date = date, current = false)!!
            view?.context?.let {
                val dao = AppDatabase.getInstance(it).balanceDao()
                dao.save(balance)
            }
        }
    }
}