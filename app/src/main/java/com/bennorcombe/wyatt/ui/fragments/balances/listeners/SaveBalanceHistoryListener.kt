package com.bennorcombe.wyatt.ui.fragments.balances.listeners

import android.app.Activity
import android.os.AsyncTask
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.navigation.NavController
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.data.Balance
import com.bennorcombe.wyatt.ui.fragments.balances.viewmodels.BalanceHistoryViewModel
import java.math.BigDecimal

class SaveBalanceHistoryListener(private val viewModel: BalanceHistoryViewModel, private val navController: NavController): View.OnClickListener {
    override fun onClick(view: View?) {
        AsyncTask.execute{
            view?.context?.let {
                val dao = AppDatabase.getInstance(it).balanceDao()
                val balance = balance()
                dao.save(balance)
            }
        }

        navController.navigate(R.id.action_balance_add_history_to_balances)
        view?.context?.let {
            val imm = it.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

    }

    private fun balance(): Balance
    {
        val amount = BigDecimal(viewModel.amount.value?.toString())
        val current = false
        val date = viewModel.date
        return viewModel.balance.value?.copy(amount = amount, current = current, date = date)?:
        Balance(amount = amount, current = current, date = date)
    }
}