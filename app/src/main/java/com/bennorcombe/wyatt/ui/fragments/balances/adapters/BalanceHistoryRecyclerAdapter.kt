package com.bennorcombe.wyatt.ui.fragments.balances.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.data.Balance
import com.bennorcombe.wyatt.ui.fragments.balances.BalancesFragmentDirections
import org.joda.time.format.DateTimeFormat
import java.math.RoundingMode

class BalanceHistoryRecyclerAdapter(private val context: Context, private val balanceHistory: List<Balance>) :
    RecyclerView.Adapter<BalanceHistoryRecyclerAdapter.ViewHolder>() {

    private val layoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = layoutInflater.inflate(R.layout.balance_history_item_list, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount() = balanceHistory.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val balance = balanceHistory[position]
        holder.textDate.text = balance.date?.toString(DateTimeFormat.forPattern("Y-MM-dd"))
        val amount = balance.amount.setScale(2, RoundingMode.HALF_EVEN)
        holder.textAmount.text = context.getString(R.string.money_amount, amount)
        holder.balanceHistoryPosition = position
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textDate: TextView = itemView.findViewById(R.id.text_balance_history_date)
        val textAmount: TextView = itemView.findViewById(R.id.text_balance_history_amount)
        var balanceHistoryPosition = 0
        init {
            itemView.setOnClickListener {
                val action = BalancesFragmentDirections.balanceHistoryAction(
                    balanceHistory[balanceHistoryPosition].id
                )
                itemView.findNavController().navigate(action)
            }
        }
    }
}