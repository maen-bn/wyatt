package com.bennorcombe.wyatt.ui.fragments.outgoings.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.data.OutgoingAndOutgoingType
import com.bennorcombe.wyatt.ui.fragments.outgoings.OutgoingsFragmentDirections
import java.text.DecimalFormat

class OutgoingsRecyclerAdapter(private val context: Context, private val outgoings: List<OutgoingAndOutgoingType>) :
    RecyclerView.Adapter<OutgoingsRecyclerAdapter.ViewHolder>() {

    private val layoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = layoutInflater.inflate(R.layout.outgoings_item_list, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount() = outgoings.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val outgoing = outgoings[position]
        holder.textOutgoingName?.text = outgoing.outgoing.name
        holder.textOutgoingAmount?.text = "£" + DecimalFormat("0.00").format(outgoing.outgoing.amount).toString()
        holder.outgoingPosition = position
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textOutgoingName = itemView.findViewById<TextView?>(R.id.text_outgoing_name)
        val textOutgoingAmount = itemView.findViewById<TextView?>(R.id.text_outgoing_amount)
        var outgoingPosition = 0

        init {
            itemView.setOnClickListener {
                val action = OutgoingsFragmentDirections.saveOutgoingAction(
                    outgoings[outgoingPosition].outgoing.id
                )
                itemView.findNavController().navigate(action)
            }
        }

    }
}