package com.bennorcombe.wyatt.ui.fragments.balances.listeners

import android.app.Activity
import android.os.AsyncTask
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.navigation.NavController
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.ui.fragments.balances.viewmodels.BalancesViewModel
import java.math.BigDecimal

class SaveCurrentBalanceListener(private val viewModel: BalancesViewModel, private val navController: NavController): View.OnClickListener {
    override fun onClick(view: View?) {
        AsyncTask.execute{
            val balance = viewModel.current.value!!
            balance.amount = BigDecimal(viewModel.amount.value?.toString())

            view?.context?.let {
                AppDatabase.getInstance(it).balanceDao().save(balance)
            }
        }


        navController.navigate(R.id.action_balances_current_to_balances)
        view?.context?.let {
            val imm = it.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}