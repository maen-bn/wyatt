package com.bennorcombe.wyatt.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bennorcombe.wyatt.R

class SetupActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setup)
    }
}