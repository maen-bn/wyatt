package com.bennorcombe.wyatt.ui.fragments.banks.listeners

import android.os.AsyncTask
import android.view.View
import androidx.navigation.NavController
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.data.Bank
import com.bennorcombe.wyatt.services.BankService
import com.bennorcombe.wyatt.ui.fragments.banks.viewmodels.SaveBankViewModel
import org.joda.time.DateTime
import java.math.BigDecimal

class SaveBankListener(
    private val viewModel: SaveBankViewModel,
    private val bankService: BankService,
    private val navController: NavController): View.OnClickListener {

    override fun onClick(view: View?) {
        AsyncTask.execute{
            view?.context?.let {
                val dao = AppDatabase.getInstance(it).bankDao()
                dao.save(bank())
                bankService.updateBalances()
            }
        }
        navController.navigate(R.id.action_global_banks)
    }

    private fun bank(): Bank {
        val name = viewModel.name.value?.toString()?:""
        val manualBalance = BigDecimal(viewModel.manualBalance.value?.toString())
        val estimatedChange = BigDecimal(viewModel.estimatedChange.value?.toString())
        val changeDay = viewModel.changeDay

        return viewModel.bank.value?.copy(
            name = name, manualBalance = manualBalance, estimatedChange = estimatedChange,
            changeDay = changeDay, estimatedChangeLastDate = DateTime.now()
        )?: Bank(
            name = name, manualBalance = manualBalance, estimatedChange = estimatedChange,
            changeDay = changeDay, estimatedChangeLastDate = DateTime.now()
        )
    }
}