package com.bennorcombe.wyatt.ui.fragments.dashboard.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bennorcombe.wyatt.R
import java.math.BigDecimal

class OverviewRecyclerAdapter(private val context: Context, private val overview: List<BigDecimal>) :
    RecyclerView.Adapter<OverviewRecyclerAdapter.ViewHolder>() {

    private val layoutInflater = LayoutInflater.from(context)

    private val names = listOf("Low", "Medium", "High")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = layoutInflater.inflate(R.layout.overview_item_list, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount() = overview.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val overview = overview[position]
        holder.textOverviewName.text = names[position]
        holder.textOverviewAmount.text = context.getString(R.string.money_amount, overview)
        if(overview < BigDecimal(0)) {
            holder.textOverviewAmount.setTextColor(context.getColor(R.color.colorDown))
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textOverviewName: TextView = itemView.findViewById(R.id.text_overview_name)
        val textOverviewAmount: TextView = itemView.findViewById(R.id.text_overview_amount)
    }
}