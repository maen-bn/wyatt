package com.bennorcombe.wyatt.ui.fragments.balances

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.databinding.FragmentBalancesCurrentBinding
import com.bennorcombe.wyatt.ui.fragments.balances.listeners.SaveCurrentBalanceListener
import com.bennorcombe.wyatt.ui.fragments.balances.viewmodels.BalancesViewModel
import com.bennorcombe.wyatt.ui.fragments.balances.viewmodels.BalancesViewModelFactory
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.math.RoundingMode

class CurrentBalanceFragment : Fragment() {

    private val viewModel: BalancesViewModel by activityViewModels {
        BalancesViewModelFactory(
            application = requireActivity().application
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentBalancesCurrentBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        val currentBalance = binding.root.findViewById<EditText>(R.id.edit_current_balance_text)
        viewModel.current.observe(viewLifecycleOwner, Observer {
            if(viewModel.amount.value.isNullOrEmpty()) {
                val amount = it.amount.setScale(2, RoundingMode.HALF_EVEN).toPlainString()
                currentBalance.text = Editable.Factory.getInstance().newEditable(amount)
            }
        })
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val navController = view.findNavController()
        val fab: FloatingActionButton = view.findViewById(R.id.fab_set_current_balance)
        fab.setOnClickListener(
            SaveCurrentBalanceListener(
                viewModel,
                navController
            )
        )
    }
}