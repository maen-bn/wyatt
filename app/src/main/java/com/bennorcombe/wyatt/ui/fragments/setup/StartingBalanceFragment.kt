package com.bennorcombe.wyatt.ui.fragments.setup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.preference.PreferenceManager
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.data.days
import com.bennorcombe.wyatt.databinding.FragmentStartingBalanceBinding
import com.bennorcombe.wyatt.ui.fragments.balances.viewmodels.BalanceHistoryViewModel
import com.bennorcombe.wyatt.ui.fragments.balances.viewmodels.BalanceHistoryViewModelFactory
import com.bennorcombe.wyatt.ui.fragments.setup.listeners.SaveStartingBalanceListener

class StartingBalanceFragment : Fragment() {

    private val viewModel: BalanceHistoryViewModel by activityViewModels {
        BalanceHistoryViewModelFactory(requireActivity().application, 0)
    }

    private val fabListener by lazy {
        SaveStartingBalanceListener(
            viewModel,
            AppDatabase.getInstance(requireContext()).balanceDao(),
            PreferenceManager.getDefaultSharedPreferences(context)
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentStartingBalanceBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        previousBalanceOnChangeListener(binding)
        binding.fabSaveStartBalance.setOnClickListener(fabListener)
        viewModel.monthsPosition.observe(viewLifecycleOwner, dateObserver())
        viewModel.yearsPosition.observe(viewLifecycleOwner, dateObserver())

        return binding.root
    }

    private fun previousBalanceOnChangeListener(binding: FragmentStartingBalanceBinding) {
        binding.todayPreviousBalanceRadioGroup.setOnCheckedChangeListener { _, _ ->
            binding.previousBalanceLayout.visibility = View.GONE
            binding.balanceAmountText.visibility = View.GONE
            if(binding.previousDateRadio.isChecked) {
                binding.previousBalanceLayout.visibility = View.VISIBLE
                binding.balanceAmountText.visibility = View.VISIBLE
            }
        }
    }

    private fun dateObserver(): Observer<Int> {
        return Observer {
            viewModel.days.postValue(days(viewModel.year!!, viewModel.month!!))
        }
    }
}