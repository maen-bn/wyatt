package com.bennorcombe.wyatt.ui.fragments.setup

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.preference.PreferenceManager
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.ui.activities.AuthActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton

class BiometricsPinInfoFragment : AppCompatDialogFragment() {

    private val preferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root =  inflater.inflate(R.layout.fragment_biometrics_pin_info, container, false)
        val fab = root.findViewById<FloatingActionButton>(R.id.fab_ask_biometrics_permission)
        fab.setOnClickListener {
            val editor = preferences.edit()
            editor.putBoolean("setup_complete", true)
            editor.apply()
            val intent = Intent(context, AuthActivity::class.java)
            startActivity(intent)
        }
        return root
    }
}