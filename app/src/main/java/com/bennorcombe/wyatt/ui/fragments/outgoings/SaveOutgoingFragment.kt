package com.bennorcombe.wyatt.ui.fragments.outgoings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.data.Outgoing
import com.bennorcombe.wyatt.data.OutgoingType
import com.bennorcombe.wyatt.databinding.FragmentOutgoingsAddBinding
import com.bennorcombe.wyatt.ui.fragments.outgoings.listeners.SaveOutgoingListener
import com.bennorcombe.wyatt.ui.fragments.outgoings.viewmodels.SaveOutgoingViewModel
import com.bennorcombe.wyatt.ui.fragments.outgoings.viewmodels.SaveOutgoingViewModelFactory
import com.google.android.material.floatingactionbutton.FloatingActionButton

class SaveOutgoingFragment : Fragment() {

    private val args: SaveOutgoingFragmentArgs by navArgs()

    private val viewModel: SaveOutgoingViewModel by viewModels {
        SaveOutgoingViewModelFactory(
            requireActivity().application,
            args.outgoingsId
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel.outgoing.observe(viewLifecycleOwner, outgoingObserver())

        val binding = FragmentOutgoingsAddBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        val spinner: Spinner = binding.root.findViewById(R.id.outgoings_type_spinner)
        spinner.adapter = outgoingsTypeAdapter(viewModel.types)

        val occurrencesLabel: TextView = binding.root.
            findViewById(R.id.outgoings_occurrences_spinner_label)
        val occurrencesSpinner: Spinner = binding.root.
            findViewById(R.id.outgoings_occurrences_spinner)

        viewModel.typePosition.observe(
            viewLifecycleOwner, typePositionObserver(occurrencesLabel, occurrencesSpinner)
        )

        return binding.root
    }

    private fun outgoingObserver(): Observer<Outgoing> {
        return Observer {
            viewModel.typePosition.postValue(
                it?.outgoingTypeId?.minus(1)?:viewModel.typePosition.value
            )
            viewModel.occurrencesPosition.postValue(
                viewModel.occurrences.indexOf(it?.occurrence?:viewModel.occurrences.first())
            )
            viewModel.name.postValue(it?.name?:viewModel.name.value)
            viewModel.amount.postValue(it?.amount?.toString()?:viewModel.amount.value)
        }
    }

    private fun typePositionObserver(label: TextView, spinner: Spinner): Observer<Int> {
        return Observer {
            label.visibility = TextView.INVISIBLE
            spinner.visibility = Spinner.INVISIBLE
            if (it == 0 || it == null) {
                label.visibility = TextView.VISIBLE
                spinner.visibility = Spinner.VISIBLE
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val navController = view.findNavController()
        val fab: FloatingActionButton = view.findViewById(R.id.fab_add_outgoing)
        fab.setOnClickListener(
            SaveOutgoingListener(
                viewModel,
                navController
            )
        )
    }

    private fun outgoingsTypeAdapter(outgoingTypes: List<OutgoingType>): ArrayAdapter<OutgoingType>? {
        val outgoingsTypeAdapter = activity?.let {
            ArrayAdapter(
                it,
                android.R.layout.simple_spinner_item,
                outgoingTypes
            )
        }
        outgoingsTypeAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        return outgoingsTypeAdapter
    }

}
