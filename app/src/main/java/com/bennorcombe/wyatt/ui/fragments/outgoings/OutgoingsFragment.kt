package com.bennorcombe.wyatt.ui.fragments.outgoings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.data.OutgoingAndOutgoingType
import com.bennorcombe.wyatt.ui.fragments.outgoings.adapters.OutgoingsRecyclerAdapter
import com.bennorcombe.wyatt.ui.fragments.outgoings.viewmodels.OutgoingsViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton

class OutgoingsFragment : Fragment() {

    private val outgoingsViewModel: OutgoingsViewModel by activityViewModels()

    private lateinit var monthlyItems: RecyclerView

    private lateinit var yearlyItems: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_outgoings, container, false)
        val fab: FloatingActionButton = root.findViewById(R.id.fab_add_outgoing)
        fab.setOnClickListener {
            val navController =
                Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
            navController.navigate(R.id.save_outgoing_action)
        }

        monthlyItems = outgoingsRecyclerView(root, R.id.monthly_outgoings_items)
        yearlyItems = outgoingsRecyclerView(root, R.id.yearly_outgoings_items)

        outgoingsViewModel.outgoings.observe(viewLifecycleOwner, outgoingsObserver())

        return root
    }

    private fun outgoingsRecyclerView(view: View, id: Int): RecyclerView{
        val recyclerView = view.findViewById<RecyclerView>(id)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        return recyclerView
    }

    private fun outgoingsObserver(): Observer<List<OutgoingAndOutgoingType>> {
        return Observer { outgoings ->
            context?.let { c ->
                monthlyItems.adapter =
                    OutgoingsRecyclerAdapter(c, outgoings.filter { o -> o.type.id == 1 })
                yearlyItems.adapter =
                    OutgoingsRecyclerAdapter(c, outgoings.filter { o -> o.type.id == 2 })
            }
        }
    }
}