package com.bennorcombe.wyatt.ui.fragments.balances.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class BalanceHistoryViewModelFactory(private val application: Application, private val balanceId: Int): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return BalanceHistoryViewModel(
            application,
            balanceId
        ) as T
    }
}