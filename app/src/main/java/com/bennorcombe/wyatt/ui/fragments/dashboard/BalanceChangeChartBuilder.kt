package com.bennorcombe.wyatt.ui.fragments.dashboard

import com.bennorcombe.wyatt.R
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import java.math.BigDecimal

class BalanceChangeChartBuilder(private val chart: LineChart) {

    fun build(changeHistory: Map<String, BigDecimal>) {
        initLineData(changeHistory)
        initXAxis(changeHistory)
        initYAxis()
        chart.description = null
        chart.legend.isEnabled = false
        chart.isAutoScaleMinMaxEnabled = true
        chart.setExtraOffsets(10F, 10F, 10F, 10F)
        chart.invalidate()
    }

    private fun initLineData(changeHistory: Map<String, BigDecimal>) {
        val entries = arrayListOf<Entry>()
        var entryIndex = 0
        changeHistory.forEach { (_, spent) ->
            entries.add(Entry(entryIndex.toFloat(), spent.toFloat()))
            entryIndex++
        }
        val dataSet = LineDataSet(entries, "")
        dataSet.color = R.color.colorPrimaryDark
        dataSet.setCircleColor(dataSet.color)
        dataSet.valueTextSize = 0f
        chart.data = LineData(dataSet)
    }

    private fun initXAxis(changeHistory: Map<String, BigDecimal>) {
        val months = changeHistory.map {
            it.key.substring(0, 3)
        }.toList()
        chart.xAxis.textSize = 12f
        chart.xAxis.granularity = 1f
        chart.xAxis.labelCount = changeHistory.count()
        chart.xAxis.valueFormatter = MyXAxisFormatter(months)
        chart.xAxis.setAvoidFirstLastClipping(true)
        chart.xAxis.position = XAxis.XAxisPosition.BOTTOM
    }

    private fun initYAxis() {
        chart.axisLeft.textSize = 12f
        chart.axisRight.isEnabled = false
        chart.axisLeft.labelCount = 8
    }

    private inner class MyXAxisFormatter(private val months: List<String>) : ValueFormatter() {
        override fun getAxisLabel(value: Float, axis: AxisBase?): String {
            return months.getOrNull(value.toInt()) ?: value.toString()
        }
    }
}