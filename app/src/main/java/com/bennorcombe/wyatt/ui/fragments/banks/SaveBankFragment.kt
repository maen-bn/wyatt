package com.bennorcombe.wyatt.ui.fragments.banks

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.data.Bank
import com.bennorcombe.wyatt.databinding.FragmentBankSaveBinding
import com.bennorcombe.wyatt.factories.BankFactory
import com.bennorcombe.wyatt.ui.fragments.banks.listeners.SaveBankListener
import com.bennorcombe.wyatt.ui.fragments.banks.viewmodels.SaveBankViewModel
import com.bennorcombe.wyatt.ui.fragments.banks.viewmodels.SaveBankViewModelFactory
import com.google.android.material.floatingactionbutton.FloatingActionButton

class SaveBankFragment : Fragment() {

    private val args: SaveBankFragmentArgs by navArgs()

    private val viewModel: SaveBankViewModel by viewModels {
        SaveBankViewModelFactory(
            args.bankId
        )
    }

    private val bankService by lazy {
        BankFactory().makeService(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel.bank.observe(viewLifecycleOwner, bankObserver())

        val binding = FragmentBankSaveBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val navController = view.findNavController()
        val fab: FloatingActionButton = view.findViewById(R.id.fab_save_bank)
        fab.setOnClickListener(SaveBankListener(viewModel, bankService, navController))
    }

    private fun bankObserver(): Observer<Bank> {
        return Observer {
            viewModel.name.postValue(it?.name?:viewModel.name.value)
            viewModel.manualBalance.postValue(
                it?.manualBalance?.toString()?:viewModel.manualBalance.value
            )
            viewModel.changeDayPosition.postValue(
                viewModel.changeDays.indexOf(it?.changeDay?:viewModel.changeDays.first())
            )
            viewModel.estimatedChange.postValue(
                it?.estimatedChange?.toString()?:viewModel.estimatedChange.value
            )
        }
    }
}
