package com.bennorcombe.wyatt.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricConstants.ERROR_NO_DEVICE_CREDENTIAL
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.preference.PreferenceManager
import com.bennorcombe.wyatt.R
import kotlin.system.exitProcess

class AuthActivity : AppCompatActivity() {

    private val preferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        if (!preferences.getBoolean("setup_complete", false)) {
            val intent = Intent(this, SetupActivity::class.java)
            startActivity(intent)
        } else {
            authenticate()
        }
    }

    private fun authenticate() {
        val executor = ContextCompat.getMainExecutor(this)
        val biometricPrompt = BiometricPrompt(this, executor, AuthCallBack())

        val promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle(getString(R.string.app_name) + " login")
            .setSubtitle("Log in using your device credentials or fingerprint")
            .setDeviceCredentialAllowed(true)
            .build()

        biometricPrompt.authenticate(promptInfo)
    }

    private inner class AuthCallBack : BiometricPrompt.AuthenticationCallback() {
        override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
            super.onAuthenticationError(errorCode, errString)
            val errorTitleView = findViewById<TextView>(R.id.authentication_error_title_text)
            val errorTextView = findViewById<TextView>(R.id.authentication_error_text)
            var errorText = errString.toString()
            if (errorCode == ERROR_NO_DEVICE_CREDENTIAL) {
                errorText = "$errorText. Please setup device credentials to use this app."
            }
            errorTitleView.visibility = View.VISIBLE
            errorTextView.text = errorText
            errorTextView.visibility = View.VISIBLE
        }

        override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
            super.onAuthenticationSucceeded(result)
            val intent = Intent(this@AuthActivity, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }

        override fun onAuthenticationFailed() {
            super.onAuthenticationFailed()
            exitApp()
        }

        private fun exitApp() {
            this@AuthActivity.finish()
            exitProcess(0)
        }
    }
}