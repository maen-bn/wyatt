package com.bennorcombe.wyatt.ui.fragments.outgoings.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class SaveOutgoingViewModelFactory(private val application: Application,  private val outgoingsId: Int): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SaveOutgoingViewModel(
            application,
            outgoingsId
        ) as T
    }
}