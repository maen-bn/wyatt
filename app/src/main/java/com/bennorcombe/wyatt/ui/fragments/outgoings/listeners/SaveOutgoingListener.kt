package com.bennorcombe.wyatt.ui.fragments.outgoings.listeners

import android.os.AsyncTask
import android.view.View
import androidx.navigation.NavController
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.data.Outgoing
import com.bennorcombe.wyatt.ui.fragments.outgoings.viewmodels.SaveOutgoingViewModel
import java.math.BigDecimal

class SaveOutgoingListener(private val viewModel: SaveOutgoingViewModel, private val navController: NavController): View.OnClickListener {
    override fun onClick(view: View?) {
        AsyncTask.execute{
            view?.context?.let {
                val dao = AppDatabase.getInstance(it).outgoingDao()
                dao.save(outgoing())
            }
        }

        navController.navigate(R.id.action_save_outgoings_to_outgoings)
    }

    private fun outgoing(): Outgoing {
        val name = viewModel.name.value?.toString()?:""
        val amount = BigDecimal(viewModel.amount.value?.toString())
        val outgoingTypeId = viewModel.typeId
        var occurrence: Int? = null
        if(outgoingTypeId == 1){
            occurrence = viewModel.occurrence
        }
        return viewModel.outgoing.value?.
            copy(name = name, amount = amount, outgoingTypeId = outgoingTypeId,
                occurrence = occurrence)?:
            Outgoing(name = name, amount = amount, outgoingTypeId = outgoingTypeId,
                occurrence = occurrence)
    }
}