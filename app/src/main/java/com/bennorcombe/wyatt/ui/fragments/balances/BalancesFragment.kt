package com.bennorcombe.wyatt.ui.fragments.balances

import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bennorcombe.wyatt.Application
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.factories.BalanceFactory
import com.bennorcombe.wyatt.ui.fragments.balances.adapters.BalanceHistoryRecyclerAdapter
import com.bennorcombe.wyatt.ui.fragments.balances.listeners.SaveCurrentToHistoryListener
import com.bennorcombe.wyatt.ui.fragments.balances.viewmodels.BalancesViewModel
import com.bennorcombe.wyatt.ui.fragments.balances.viewmodels.BalancesViewModelFactory
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.invoke
import kotlinx.coroutines.launch
import java.math.RoundingMode

class BalancesFragment : Fragment() {

    private lateinit var currentBalanceText: TextView
    private lateinit var updatingBalanceProgressBar: ProgressBar

    private val viewModel: BalancesViewModel by activityViewModels {
        BalancesViewModelFactory(
            application = requireActivity().application
        )
    }

    private val navController by lazy {
        Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
    }

    private val preferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(requireContext())
    }

    private val balanceService by lazy {
        val balanceFactory = BalanceFactory()
        balanceFactory.makeService(requireContext())
    }

    private val uiCoroutineScope = CoroutineScope(Dispatchers.Main)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_balances, container, false)
        currentBalanceText = root.findViewById(R.id.text_current_balance)
        updatingBalanceProgressBar = root.findViewById(R.id.updating_balance_progress_bar)
        viewModel.current.observe(viewLifecycleOwner, Observer {
            val amount = it.amount.setScale(2, RoundingMode.HALF_EVEN)
            currentBalanceText.text = getString(R.string.money_amount, amount)
        })
        setRecyclerView(root)
        setFabListeners(root)
        return root
    }

    private fun setRecyclerView(root: View) {
        val recyclerView = root.findViewById<RecyclerView>(R.id.balance_history_items)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        viewModel.history.observe(viewLifecycleOwner, Observer { balanceHistory ->
            context?.let { c ->
                recyclerView.adapter = BalanceHistoryRecyclerAdapter(c, balanceHistory)
            }
        })
    }

    private fun setFabListeners(view: View) {
        val editFab = findFab(R.id.fab_edit_balance, view)
        val updateFab = findFab(R.id.fab_update_balance_from_banks, view)
        val historyFab = findFab(R.id.fab_add_balance_history, view)
        val currentToHistoryFab = findFab(R.id.fab_current_balance_to_history, view)
        setFabVisibility(editFab, updateFab)
        editFab.setOnClickListener { navigate(R.id.action_balances_to_balances_current) }
        updateFab.setOnClickListener { uiCoroutineScope.launch { loadNewBalance() } }
        currentToHistoryFab.setOnClickListener(SaveCurrentToHistoryListener(viewModel))
        historyFab.setOnClickListener { navigate(R.id.balance_history_action) }
    }

    private fun findFab(id: Int, view: View): FloatingActionButton
    {
        return view.findViewById(id)
    }

    private fun setFabVisibility(editFab: FloatingActionButton, updateFab: FloatingActionButton) {
        if(preferences.getBoolean("auto_balance_preference", false)) {
            editFab.hide()
            updateFab.show()
        }
    }

    private fun navigate(id: Int, bundle: Bundle? = null)
    {
        navController.navigate(id, bundle)
    }

    private suspend fun loadNewBalance() {
        currentBalanceText.visibility = View.INVISIBLE
        updatingBalanceProgressBar.visibility = View.VISIBLE
        updateBalance()
        currentBalanceText.visibility = View.VISIBLE
        updatingBalanceProgressBar.visibility = View.INVISIBLE
    }

    private suspend fun updateBalance() = Dispatchers.IO { balanceService.update() }
}