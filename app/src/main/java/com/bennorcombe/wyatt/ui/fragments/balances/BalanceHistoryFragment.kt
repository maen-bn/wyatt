package com.bennorcombe.wyatt.ui.fragments.balances

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.data.Balance
import com.bennorcombe.wyatt.databinding.FragmentBalanceAddHistoryBinding
import com.bennorcombe.wyatt.ui.fragments.balances.listeners.SaveBalanceHistoryListener
import com.bennorcombe.wyatt.ui.fragments.balances.viewmodels.BalanceHistoryViewModel
import com.bennorcombe.wyatt.ui.fragments.balances.viewmodels.BalanceHistoryViewModelFactory
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.math.RoundingMode

class BalanceHistoryFragment : Fragment() {

    private val args: BalanceHistoryFragmentArgs by navArgs()

    val viewModel: BalanceHistoryViewModel by viewModels {
        BalanceHistoryViewModelFactory(
            requireActivity().application, args.balanceId
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel.balance.observe(viewLifecycleOwner, balanceObserver())

        val binding = FragmentBalanceAddHistoryBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val navController = view.findNavController()
        val fab: FloatingActionButton = view.findViewById(R.id.fab_add_balance_history)
        fab.setOnClickListener(
            SaveBalanceHistoryListener(
                viewModel,
                navController
            )
        )
    }

    private fun balanceObserver(): Observer<Balance>
    {
        return Observer {
            val monthPos = getMonthPosition(it)
            var yearPos = viewModel.years.indexOf(it?.date?.toString("YYYY")?.toInt())
            if (yearPos == -1) yearPos = viewModel.yearsPosition.value?:0
            viewModel.monthsPosition.postValue(monthPos)
            viewModel.yearsPosition.postValue(yearPos)
            viewModel.amount.postValue(formatAmount(it))
        }
    }

    private fun getMonthPosition(balance: Balance?): Int {
        val balanceMonthNumber = balance?.date?.toString("M")?.toInt()
        return balanceMonthNumber?.minus(1) ?: viewModel.monthsPosition.value ?: 0
    }

    private fun formatAmount(balance: Balance?): String? {
        val amount = balance?.amount?.setScale(2, RoundingMode.HALF_EVEN)
        return amount?.toString()?:viewModel.amount.value
    }

}
