package com.bennorcombe.wyatt.ui.fragments.banks

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.factories.BankFactory
import com.bennorcombe.wyatt.ui.fragments.banks.adapters.BanksRecyclerAdapter
import com.bennorcombe.wyatt.ui.fragments.banks.viewmodels.BanksViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton

class BanksFragment : Fragment() {

    private val viewModel: BanksViewModel by viewModels()

    private val bankService by lazy {
        BankFactory().makeService(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_banks, container, false)
        val fab = view.findViewById<FloatingActionButton>(R.id.fab_add_bank)
        fab.setOnClickListener {
            bankService.add(fragment = this, onCancelled =  this::navigateToManualSave)
        }
        val recyclerView = view.findViewById<RecyclerView>(R.id.banks_items)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        viewModel.banks.observe(viewLifecycleOwner, Observer {
            recyclerView.adapter = BanksRecyclerAdapter(this, bankService, it)
        })
        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        bankService.onAddResult(requestCode, resultCode, data)
    }

    private fun navigateToManualSave() {
        findNavController().navigate(R.id.action_global_save_bank)
    }
}
