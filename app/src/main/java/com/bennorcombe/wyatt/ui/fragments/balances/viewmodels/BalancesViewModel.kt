package com.bennorcombe.wyatt.ui.fragments.balances.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.bennorcombe.wyatt.data.AppDatabase

class BalancesViewModel(application: Application) : AndroidViewModel(application) {
    val current = AppDatabase.getInstance(application).balanceDao().current()

    val history = AppDatabase.getInstance(application).balanceDao().history()

    var amount = MutableLiveData<String>()
}