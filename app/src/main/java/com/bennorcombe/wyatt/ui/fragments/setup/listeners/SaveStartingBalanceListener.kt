package com.bennorcombe.wyatt.ui.fragments.setup.listeners

import android.content.SharedPreferences
import android.view.View
import android.widget.RadioButton
import androidx.navigation.findNavController
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.data.Balance
import com.bennorcombe.wyatt.data.BalanceDao
import com.bennorcombe.wyatt.ui.fragments.balances.viewmodels.BalanceHistoryViewModel
import kotlinx.coroutines.*
import org.joda.time.DateTime
import java.math.BigDecimal

class SaveStartingBalanceListener(
    private val viewModel: BalanceHistoryViewModel,
    private val balanceDao: BalanceDao,
    private val sharedPreferences: SharedPreferences
    ): View.OnClickListener {

    private val uiCoroutineScope = CoroutineScope(Dispatchers.Main)

    private var newMonthDay = DateTime.now().dayOfMonth

    override fun onClick(view: View?) {
        uiCoroutineScope.launch {
            val previousBalance = view!!.rootView.findViewById<RadioButton>(R.id.previous_date_radio)
            saveBalances(previousBalance.isChecked)
            val nav = view.findNavController()
            nav.navigate(R.id.action_starting_balance_to_biometrics_pin_info)

        }
    }

    @ExperimentalCoroutinesApi
    private suspend fun saveBalances(hasPreviousBalance: Boolean) = Dispatchers.IO {
        saveBalance()
        if(hasPreviousBalance) {
            savePreviousBalance()
        }
        saveNewMonthDay()
    }

    private fun saveBalance() {
        val amount = BigDecimal(viewModel.amount.value?:"0")
        val balance = Balance(amount = amount, date = null)
        balanceDao.save(balance)
    }

    private fun savePreviousBalance() {
        val prevAmount = BigDecimal(viewModel.previousDateAmount.value?:"0")
        val prevBalance = Balance(amount = prevAmount, date = viewModel.date, current = false)
        newMonthDay = viewModel.date.dayOfMonth
        balanceDao.save(prevBalance)
    }

    private fun saveNewMonthDay() {
        val editor = sharedPreferences.edit()
        editor.putInt("new_month_day", newMonthDay)
        editor.apply()
    }
}