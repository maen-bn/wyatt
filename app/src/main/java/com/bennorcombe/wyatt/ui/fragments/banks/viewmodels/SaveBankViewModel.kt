package com.bennorcombe.wyatt.ui.fragments.banks.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bennorcombe.wyatt.Application
import com.bennorcombe.wyatt.data.AppDatabase

class SaveBankViewModel(bankId: Int) : ViewModel() {
    private val db = AppDatabase.getInstance(Application()).bankDao()

    val bank = db.find(bankId)

    val name = MutableLiveData<String>()

    val manualBalance = MutableLiveData<String>()

    val estimatedChange = MutableLiveData<String>()

    var changeDayPosition = MutableLiveData(0)

    val changeDays = listOf(1..14).flatten()

    val changeDay
        get() = changeDayPosition.value?.let {
            changeDays[it]
        }?:0
}
