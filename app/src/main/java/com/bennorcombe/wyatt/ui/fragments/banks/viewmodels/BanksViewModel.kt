package com.bennorcombe.wyatt.ui.fragments.banks.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bennorcombe.wyatt.Application
import com.bennorcombe.wyatt.data.AppDatabase
import java.math.BigDecimal

class BanksViewModel : ViewModel() {
    private val db = AppDatabase.getInstance(Application()).bankDao()

    val banks = db.all()

    val balance : MutableLiveData<BigDecimal> = MutableLiveData()
}
