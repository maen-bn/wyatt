package com.bennorcombe.wyatt.ui.fragments.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.data.Balance
import com.bennorcombe.wyatt.data.OutgoingAndOutgoingType
import com.bennorcombe.wyatt.services.BudgetService
import com.bennorcombe.wyatt.services.OverviewService
import com.bennorcombe.wyatt.ui.fragments.dashboard.adapters.OverviewRecyclerAdapter
import com.bennorcombe.wyatt.ui.fragments.dashboard.viewmodels.DashboardViewModel
import com.bennorcombe.wyatt.ui.fragments.dashboard.viewmodels.DashboardViewModelFactory
import java.math.BigDecimal

class DashboardFragment : Fragment() {

    private val viewModel: DashboardViewModel by viewModels {
        DashboardViewModelFactory(
            requireActivity().application
        )
    }

    private lateinit var monthOverviewRecycler: RecyclerView

    private lateinit var yearOverviewRecycler: RecyclerView

    private lateinit var chartBuilder: BalanceChangeChartBuilder

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        monthOverviewRecycler = initOverviewRecycler(root, R.id.current_month_overview)
        yearOverviewRecycler = initOverviewRecycler(root, R.id.current_year_overview)
        chartBuilder = BalanceChangeChartBuilder(root.findViewById(R.id.chart))
        viewModel.outgoings.observe(viewLifecycleOwner, overviewObserver())

        return root
    }

    private fun initOverviewRecycler(view: View, id: Int): RecyclerView {
        val recycler = view.findViewById<RecyclerView>(id)
        recycler.layoutManager = GridLayoutManager(activity, 3)
        return recycler
    }

    private fun overviewObserver(): Observer<List<OutgoingAndOutgoingType>> {
        return Observer {
            val service = OverviewService(BudgetService(it))
            viewModel.balances.observe(viewLifecycleOwner, balancesObserver(service))
            viewModel.history.observe(viewLifecycleOwner, historyObserver(service))
        }
    }

    private fun balancesObserver(service: OverviewService): Observer<List<Balance>> {
        return Observer { balances ->
            if (balances.size > 1) {
                monthOverviewRecycler.adapter = OverviewRecyclerAdapter(
                    requireContext(), service.month(balances.first(), balances[1])
                )
                yearOverviewRecycler.adapter = OverviewRecyclerAdapter(
                    requireContext(), service.all(balances.filter { !it.current })
                )
                setUpGainLoss(
                    service.totalGainLoss(balances),
                    service.averageGainLoss(balances),
                    service.predictedSpend(balances)
                )
            }
        }
    }

    private fun historyObserver(service: OverviewService): Observer<List<Balance>> {
        return Observer { balanceHistory ->
            if (balanceHistory.size > 2) {
                chartBuilder.build(service.balanceChangeHistory(balanceHistory))
            }
        }
    }

    private fun setUpGainLoss(total: BigDecimal, avg: BigDecimal, predicted: BigDecimal) {
        setGainLossText(R.id.total_gain_loss_amount, R.id.total_gain_loss_amount_summary, total)
        setGainLossText(R.id.average_gain_loss_amount, R.id.average_gain_loss_amount_summary, avg)
        setGainLossText(
            R.id.predicted_year_gain_loss_amount,
            R.id.predicted_year_gain_loss_amount_summary,
            predicted
        )
    }

    private fun setGainLossText(id: Int, summaryId: Int, gainLoss: BigDecimal) {
        val summaryTextView = view?.findViewById<TextView>(summaryId)
        when(gainLoss.compareTo(BigDecimal(0))) {
            1 -> {
                summaryTextView?.text = "(Up)"
                summaryTextView?.setTextColor(requireContext().getColor(R.color.colorUp))
            }
            -1 -> {
                summaryTextView?.text = "(Down)"
                summaryTextView?.setTextColor(requireContext().getColor(R.color.colorDown))
            }
        }
        val gainLossTextView = view?.findViewById<TextView>(id)
        gainLossTextView?.text = getString(R.string.money_amount, gainLoss)

    }
}