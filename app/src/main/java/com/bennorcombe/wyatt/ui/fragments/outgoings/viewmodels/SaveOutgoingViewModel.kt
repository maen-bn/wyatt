package com.bennorcombe.wyatt.ui.fragments.outgoings.viewmodels

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.data.OutgoingType

class SaveOutgoingViewModel(application: Application, outgoingId: Int) : AndroidViewModel(application) {

    private val db: AppDatabase = AppDatabase.getInstance(application)

    val outgoing = db.outgoingDao().find(outgoingId)

    lateinit var types: List<OutgoingType>

    init {
        AsyncTask.execute{
            types = db.outgoingsTypeDao().all()
        }
    }

    var name = MutableLiveData<String>()

    var amount = MutableLiveData<String>()

    var typePosition = MutableLiveData(0)

    var typeId: Int
        get() =
            typePosition.value?.let {
                types[it].id
            }!!
        set(value) {
            val position = types.indexOfFirst {
                it.id == value
            }
            if (position != -1) {
                typePosition.value = position + 1
            }
        }
    val type
        get() =
            typePosition.value?.let {
                types[it]
            }

    var occurrencesPosition = MutableLiveData(0)

    val occurrences = listOf(12,11,10,9,8,7,6,5,4,3,2,1)

    val occurrence
        get() = occurrencesPosition.value?.let {
            occurrences[it]
        }?:0
}
