package com.bennorcombe.wyatt.ui.fragments.dashboard.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.data.endDate
import com.bennorcombe.wyatt.data.startDate

class DashboardViewModel(application: Application) : AndroidViewModel(application) {
    private val db = AppDatabase.getInstance(application)
    val balances = db.balanceDao().allByRange(startDate(), endDate())
    val history = db.balanceDao().history()
    val outgoings = db.outgoingDao().getOutgoingsAndType()
}