package com.bennorcombe.wyatt.ui.fragments.outgoings.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import com.bennorcombe.wyatt.data.AppDatabase

class OutgoingsViewModel : ViewModel() {
    val outgoings = AppDatabase.getInstance(Application()).outgoingDao().getOutgoingsAndType()
}