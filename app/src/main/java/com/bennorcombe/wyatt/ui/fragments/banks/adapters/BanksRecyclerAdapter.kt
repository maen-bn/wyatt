package com.bennorcombe.wyatt.ui.fragments.banks.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.data.Bank
import com.bennorcombe.wyatt.services.BankService
import com.bennorcombe.wyatt.ui.fragments.banks.BanksFragmentDirections

class BanksRecyclerAdapter(
    private val fragment: Fragment,
    private val service: BankService,
    private val banks: List<Bank>) : RecyclerView.Adapter<BanksRecyclerAdapter.ViewHolder>() {

    private val layoutInflater = LayoutInflater.from(fragment.context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = layoutInflater.inflate(R.layout.bank_item_list, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount() = banks.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val bank = banks[position]
        holder.textBankName.text = bank.name
        if (bank.authUpdateRequired) {
            holder.refreshButton.visibility = ImageButton.VISIBLE
        }
        if (bank.isManualBank()) {
            holder.editButton.visibility = ImageButton.VISIBLE
        }
        holder.refreshButton.setOnClickListener {
            service.update(fragment, bank)
        }
        holder.editButton.setOnClickListener {
            val action = BanksFragmentDirections.saveBankAction(bank.id)
            holder.itemView.findNavController().navigate(action)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textBankName: TextView = itemView.findViewById(R.id.text_bank_name)
        val refreshButton: ImageButton = itemView.findViewById(R.id.bank_refresh_button)
        val editButton: ImageButton = itemView.findViewById(R.id.bank_edit_button)
    }
}