package com.bennorcombe.wyatt.ui.fragments.settings

import android.content.Intent
import android.os.Bundle
import androidx.navigation.Navigation
import androidx.preference.*
import com.bennorcombe.wyatt.BuildConfig
import com.bennorcombe.wyatt.R
import com.bennorcombe.wyatt.ui.activities.MainActivity

class SettingsFragment: PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
        bankLinkSetup()
        screenshotSetup()
    }

    private fun bankLinkSetup() {
        val addBankLink = findPreference<Preference>("add_bank_link")
        addBankLink?.setOnPreferenceClickListener {
            val n = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
            n.navigate(R.id.action_global_banks)
            true
        }
        hideAutoBalanceIfRequired()
    }

    private fun hideAutoBalanceIfRequired() {
        if (!BuildConfig.PLAID_ENABLED) {
            val preferenceScreen = findPreference<PreferenceScreen>("preferences")
            val autoBalanceCategory = findPreference<PreferenceCategory>("auto_balance_category")
            preferenceScreen?.removePreference(autoBalanceCategory)
        }
    }

    private fun screenshotSetup() {
        val screenshots = findPreference<SwitchPreference>("allow_screenshots")
        screenshots?.setOnPreferenceClickListener {
            val intent = Intent(context, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            true
        }
    }
}