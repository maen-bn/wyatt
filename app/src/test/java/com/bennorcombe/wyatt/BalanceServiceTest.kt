package com.bennorcombe.wyatt

import com.bennorcombe.wyatt.data.Balance
import com.bennorcombe.wyatt.data.BalanceDao
import com.bennorcombe.wyatt.data.Bank
import com.bennorcombe.wyatt.services.*
import org.joda.time.DateTime
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.atLeastOnce
import org.mockito.Mockito.verify
import java.math.BigDecimal

class BalanceServiceTest {
    private val balanceDao = Mockito.mock(BalanceDao::class.java)
    private val bankService = Mockito.mock(BankService::class.java)
    private val notificationService = Mockito.mock(NotificationService::class.java)
    private lateinit var service: BalanceService
    private lateinit var balance: Balance

    @Before
    fun setUp() {
        balance = Balance(amount = BigDecimal(0), date = DateTime())
        setUpStubs()
        service = BalanceService(balanceDao, bankService, notificationService)
    }

    @Test
    fun testBalanceIsSavedCorrectly() {
        successfulBalanceUpdateStub()
        service.update()
        Assert.assertEquals(BigDecimal(176), balance.amount)
    }

    @Test
    fun testFailedUpdateRollsBackBalanceToPreviousState() {
        unsuccessfulBalanceUpdateStub()
        service.update()
        Assert.assertEquals(BigDecimal(0), balance.amount)
    }

    @Test
    fun testFailedUpdateNotifiesUser() {
        unsuccessfulBalanceUpdateStub()
        service.update()
        verify(notificationService, atLeastOnce()).notifyBankAuthUpdate(any())
    }

    private fun setUpStubs() {
        Mockito.`when`(balanceDao.currentNow()).thenReturn(balance)
        Mockito.`when`(balanceDao.save(any())).then {
            balance = (it.arguments[0] as Balance)
            Unit
        }
    }

    private fun successfulBalanceUpdateStub() {
        Mockito.`when`(bankService.totalBankBalance()).thenReturn(BigDecimal(176))
    }

    private fun unsuccessfulBalanceUpdateStub() {
        Mockito.`when`(bankService.totalBankBalance()).thenAnswer {
            throw BankBalanceException(Bank(name = "Fake Bank"))
        }
    }
}