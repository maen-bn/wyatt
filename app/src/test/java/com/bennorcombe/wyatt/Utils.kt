package com.bennorcombe.wyatt

import org.mockito.Mockito
import java.io.IOException
import java.io.InputStream

fun <T> any(): T = Mockito.any<T>()

@Throws(IOException::class)
fun <T>readFromFile(filename: String, context: Class<T>): String {
    val s: InputStream = context.getResourceAsStream(filename)!!
    val stringBuilder = StringBuilder()
    var i: Int
    val b = ByteArray(4096)
    while (s.read(b).also { i = it } != -1) {
        stringBuilder.append(String(b, 0, i))
    }
    return stringBuilder.toString()
}