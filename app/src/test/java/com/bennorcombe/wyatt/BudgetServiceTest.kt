package com.bennorcombe.wyatt

import com.bennorcombe.wyatt.services.BudgetService
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal
import java.math.RoundingMode

class BudgetServiceTest {

    private lateinit var service: BudgetService

    @Before
    fun initService() {
        service = BudgetService(fakeOutgoings())
    }

    @Test
    fun testMonthlyLowBudget() {
        assertEquals(
            BigDecimal(206.71).setScale(2, RoundingMode.HALF_EVEN),
            service.monthlyLow()
        )
    }

    @Test
    fun testMonthlyHighBudget() {
        assertEquals(
            BigDecimal(981.87).setScale(2, RoundingMode.HALF_EVEN),
            service.monthlyHigh()
        )
    }

    @Test
    fun testMonthlyMedianBudget() {
        assertEquals(
            BigDecimal(594.29).setScale(2, RoundingMode.HALF_EVEN),
            service.monthlyMedian()
        )
    }
}