package com.bennorcombe.wyatt

import com.bennorcombe.wyatt.data.Balance
import com.bennorcombe.wyatt.services.BudgetService
import com.bennorcombe.wyatt.services.OverviewService
import org.joda.time.DateTime
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal
import java.math.RoundingMode

class OverviewServiceTest {

    private lateinit var service: OverviewService
    private lateinit var balances: List<Balance>

    @Before
    fun setUp() {
        service = OverviewService(BudgetService(fakeOutgoings()))
        val first = Balance(1, BigDecimal(4000), true, null)
        val second = Balance(2, BigDecimal(4500), false, DateTime().minusMonths(1))
        val third = Balance(3, BigDecimal(5000), false, DateTime().minusMonths(2))

        balances = listOf(first, second, third)
    }

    @Test
    fun testMonth() {
        val month = service.month(balances[0], balances[1])

        assertEquals(BigDecimal(-293.29).setScale(2, RoundingMode.HALF_EVEN), month[0])
        assertEquals(BigDecimal(94.29).setScale(2, RoundingMode.HALF_EVEN), month[1])
        assertEquals(BigDecimal(481.87).setScale(2, RoundingMode.HALF_EVEN), month[2])
    }

    @Test
    fun testAll() {
        val all = service.all(balances)

        assertEquals(BigDecimal(-586.58).setScale(2, RoundingMode.HALF_EVEN), all[0])
        assertEquals(BigDecimal(188.58).setScale(2, RoundingMode.HALF_EVEN), all[1])
        assertEquals(BigDecimal(963.74).setScale(2, RoundingMode.HALF_EVEN), all[2])
    }

    @Test
    fun testTotalGainLoss() {
        val total = service.totalGainLoss(balances)

        assertEquals(BigDecimal(1000).setScale(2, RoundingMode.HALF_EVEN), total)
    }

    @Test
    fun testAverageGainLoss() {
        val average = service.averageGainLoss(balances)

        assertEquals(BigDecimal(500).setScale(2, RoundingMode.HALF_EVEN), average)
    }

    @Test
    fun testPredicatedGainLoss() {
        val predicated = service.predictedSpend(balances)

        assertEquals(BigDecimal(6000.00).setScale(2, RoundingMode.HALF_EVEN), predicated)
    }

    @Test
    fun testBalanceChangeHistory() {
        val b = balances.toMutableList()
        b.add(Balance(5, BigDecimal(6000), false, DateTime().minusMonths(3)))
        val history  = service.balanceChangeHistory(b).toList()
        assertEquals(BigDecimal(-1000), history[0].second)
        assertEquals(BigDecimal(-500), history[1].second)
    }

    @Test
    fun testBalanceChangeHistoryFillsInMissingMonths() {
        val b = balances.toMutableList()
        b.add(Balance(5, BigDecimal(6000), false, DateTime().minusMonths(5)))
        val history  = service.balanceChangeHistory(b).toList()
        val expectedChange = BigDecimal(-1000)
        assertEquals(expectedChange, history[0].second)
        assertEquals(expectedChange, history[1].second)
        assertEquals(expectedChange, history[2].second)
    }

    @Test
    fun testBalanceChangeHistoryIsInOrderWhenMissingMonthsAreAddedIn() {
        val b = balances.toMutableList()
        b.add(Balance(5, BigDecimal(6000), false, DateTime().minusMonths(5)))
        val months  = service.balanceChangeHistory(b).keys.toList()
        listOf(4,3,2,1).map {
            DateTime.now().minusMonths(it).toString("MMMM")
        }.forEachIndexed { index, expectedMonth ->
            assertEquals(expectedMonth, months[index])
        }
    }
}