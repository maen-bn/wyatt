package com.bennorcombe.wyatt

import com.bennorcombe.wyatt.data.api.AccessTokenResponse
import com.bennorcombe.wyatt.services.BankApiService
import com.bennorcombe.wyatt.services.PlaidApiService
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.json.JSONArray
import org.json.JSONObject
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class BankApiServiceTest {

    private lateinit var mockWebServer: MockWebServer

    private lateinit var service: BankApiService

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        service = PlaidApiService(
            OkHttpClient(),
            mockWebServer.url("").toString().replace("/", "")
        )
    }

    @After
    fun shutdownMockServer() {
        mockWebServer.shutdown()
    }

    @Test
    fun testAccessTokenCanBeRetrieveFromResponse() {
        enqueueResponse(200, "/access_token.json")
        runBlocking {
            val accessToken = accessTokenSync(service, "public-fds7fs3h").token
            assert(accessToken.isNotEmpty())
        }
    }

    @Test
    fun testPublicTokenCanBeRetrieveFromResponse() {
        enqueueResponse(200, "/public_token.json")
        runBlocking {
            val publicToken = publicTokenSync(service, "access_token-4234234")
                .get("public_token").toString()
            assert(publicToken.isNotEmpty())
        }
    }

    @Test
    fun testBalanceValueCanBeRetrieveFromResponse() {
        enqueueResponse(200, "/balances.json")
        val response = service.balance("access-token-42342342")
        val balance = (((response.get("accounts") as JSONArray)[0] as JSONObject)
            .get("balances") as JSONObject).get("available").toString()
        assert(balance.isNotEmpty())
    }

    private fun enqueueResponse(code: Int, jsonSampleName: String) {
        val mockResponse = MockResponse()
        mockResponse.setResponseCode(code)
        mockResponse.setBody(readFromFile(jsonSampleName, javaClass))
        mockWebServer.enqueue(mockResponse)
    }

    /**
     * The API service makes some HTTP requests asynchronously. In order to test the service, the
     * services methods needs to be wrapped in a coroutine and then ran using "runBlocking"
     */
    private suspend fun accessTokenSync(client: BankApiService, publicToken: String): AccessTokenResponse
            = suspendCoroutine { cont -> client.accessToken(publicToken) { cont.resume(it) } }

    private suspend fun publicTokenSync(client: BankApiService, accessToken: String): JSONObject
            = suspendCoroutine { cont -> client.publicToken(accessToken) { cont.resume(it) } }
}