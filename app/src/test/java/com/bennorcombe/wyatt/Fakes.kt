package com.bennorcombe.wyatt

import com.bennorcombe.wyatt.data.Outgoing
import com.bennorcombe.wyatt.data.OutgoingAndOutgoingType
import com.bennorcombe.wyatt.data.OutgoingType
import java.math.BigDecimal

val monthlyType = OutgoingType(1, "Monthly Outgoing")
val yearlyType = OutgoingType(2, "Yearly Outgoing")

fun fakeOutgoings(): List<OutgoingAndOutgoingType> {
    return listOf(
        OutgoingAndOutgoingType(Outgoing(1, "Test1", BigDecimal(10.00),1), monthlyType),
        OutgoingAndOutgoingType(Outgoing(2, "Test2", BigDecimal(100.56),1), monthlyType),
        OutgoingAndOutgoingType(Outgoing(3, "Test3", BigDecimal(50.45),1), monthlyType),
        OutgoingAndOutgoingType(Outgoing(4, "Test4", BigDecimal(40.20),1), monthlyType),
        OutgoingAndOutgoingType(Outgoing(5, "Test5", BigDecimal(5.50),1), monthlyType),
        OutgoingAndOutgoingType(Outgoing(6, "Test6", BigDecimal(600.56),2), yearlyType),
        OutgoingAndOutgoingType(Outgoing(7, "Test7", BigDecimal(300),2), yearlyType),
        OutgoingAndOutgoingType(Outgoing(8, "Test8", BigDecimal(3900.77),2), yearlyType),
        OutgoingAndOutgoingType(Outgoing(9, "Test9", BigDecimal(3500.56),2), yearlyType),
        OutgoingAndOutgoingType(Outgoing(10, "Test10", BigDecimal(1000),2), yearlyType)
    )
}