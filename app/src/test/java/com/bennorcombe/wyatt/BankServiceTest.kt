package com.bennorcombe.wyatt

import androidx.fragment.app.Fragment
import com.bennorcombe.wyatt.data.Bank
import com.bennorcombe.wyatt.data.BankDao
import com.bennorcombe.wyatt.data.api.AccessTokenResponse
import com.bennorcombe.wyatt.services.BankApiService
import com.bennorcombe.wyatt.services.BankBalanceException
import com.bennorcombe.wyatt.services.BankOAuthService
import com.bennorcombe.wyatt.services.PlaidBankService
import com.bennorcombe.wyatt.workers.ScheduleBalanceUpdate
import org.joda.time.DateTime
import org.joda.time.DateTimeUtils
import org.json.JSONObject
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import java.math.BigDecimal

class BankServiceTest {

    private val oAuthService = Mockito.mock(BankOAuthService::class.java)

    private val apiService = Mockito.mock(BankApiService::class.java)

    private val bankDao = Mockito.mock(BankDao::class.java)

    private val scheduleBalanceUpdate = Mockito.mock(ScheduleBalanceUpdate::class.java)

    private val service = PlaidBankService(
        oAuthService,
        apiService,
        bankDao,
        scheduleBalanceUpdate
    )

    private val fakePublicToken = "public-token-testing"

    private val fakeAccessToken = "access-token-testing"

    private val fakeBankName = "Test Bank"

    private val fakeBankBalance = BigDecimal(10000)

    private val manualBankBalance = BigDecimal(5000)

    private val manualBankBalanceChange = BigDecimal(200)

    private val accessTokenJson = JSONObject("{\"item_id\": \"4gu8fh48sdfhsdfsdo\"}")

    private val publicTokenJson = JSONObject("{\"public_token\": \"$fakePublicToken\"}")

    private val fixedDateTime = DateTime.parse("${DateTime.now().year}-05-05")

    private val banks = listOf(
        Bank(name = fakeBankName, externalId = externalId()),
        Bank(name = "Test Manual Bank",
            estimatedChange = manualBankBalanceChange,
            estimatedChangeLastDate = fixedDateTime,
            manualBalance = manualBankBalance
        )
    )

    @Before
    fun setupStubs() {
        `when`(oAuthService.auth(any(), any(), any())).then{ invocation ->
            (invocation.arguments[1] as (String, String) -> Unit)(fakePublicToken, fakeBankName)
            true
        }
        `when`(apiService.accessToken(any(), any())).then{ invocation ->
            (invocation.arguments[1] as (AccessTokenResponse) -> Unit)(
                AccessTokenResponse(fakeAccessToken, accessTokenJson)
            )
        }
        `when`(apiService.publicToken(any(), any())).then {invocation ->
            (invocation.arguments[1] as (JSONObject) -> Unit)(publicTokenJson)
        }
    }

    @Test
    fun verifyAddSavesBankWithExternalId() {
        service.add(Fragment()) {}
        verify(bankDao).save(
            Bank(externalId = externalId(), name = fakeBankName)
        )
    }

    @Test
    fun verifyAddWillSaveAccessToken() {
        service.add(Fragment()) {}
        verify(oAuthService).saveAccessToken(
            fakeAccessToken, externalId()
        )
    }

    @Test
    fun updateReturnsFalseWhenBankHasNoExternalId() {
        assertFalse(service.update(Fragment(), Bank(name = fakeBankName)))
    }

    @Test
    fun updateReturnsFalseWhenABankHasNoAccessToken() {
        assertFalse(
            service.update(Fragment(), Bank(name = fakeBankName, externalId = externalId()))
        )
    }

    @Test
    fun updateReAuthenticatesBank() {
        val bank = Bank(externalId = externalId(), name = fakeBankName)
        stubOAuthAccessToken()
        assertTrue(service.update(Fragment(), bank))
        verify(bankDao).save(bank.copy(authUpdateRequired = false))
    }

    @Test
    fun testHandleBankBalancesCanBeUsedToAddUpTheBalanceCorrectly() {
        `when`(bankDao.allNow()).thenReturn(banks)
        stubOAuthAccessToken()
        stubApiBalance()

        val expectedTotalBalance = fakeBankBalance + fakeBankBalance + manualBankBalance

        var actualTotalBalance = BigDecimal(0)
        actualTotalBalance += service.totalBankBalance()

        assertEquals(expectedTotalBalance, actualTotalBalance)
    }

    @Test
    fun testHandleBankBalanceCanBeUsedToAddUpTheTotalBalanceWhenAManualBankBalanceNeedsUpdating() {
        // Part of the condition for a manual bank's balance being updated is if the current
        // datetime day is < than start day of a new month. As the start day of a new month
        // currently can't be changed, the current date time needs to be fixed for the test
        DateTimeUtils.setCurrentMillisFixed(fixedDateTime.millis)

        val mutableBanks = banks.toMutableList()
        mutableBanks[1] = mutableBanks[1].copy(changeDay = DateTime().dayOfWeek, estimatedChangeLastDate = fixedDateTime.minusMonths(1))
        `when`(bankDao.allNow()).thenReturn(mutableBanks)
        stubOAuthAccessToken()
        stubApiBalance()

        val manualBalance = manualBankBalance + manualBankBalanceChange
        val expectedTotalBalance = fakeBankBalance + fakeBankBalance + manualBalance

        var actualTotalBalance = BigDecimal(0)
        actualTotalBalance += service.totalBankBalance()

        assertEquals(expectedTotalBalance, actualTotalBalance)
        verify(bankDao).save(mutableBanks[1].copy(manualBalance = manualBalance))
    }

    @Test
    fun testHandleBankBalancesWillFlaggedBankForReAuthenticationWhenNeeded() {
        `when`(bankDao.allNow()).thenReturn(banks.filter { !it.isManualBank() })
        stubOAuthAccessToken()
        stubApiBalanceError()

        try {
            service.totalBankBalance()
        } catch (e: BankBalanceException) {}
        verify(bankDao).save(banks.first().copy(authUpdateRequired = true))
    }

    private fun externalId() = accessTokenJson.get("item_id").toString()

    private fun stubOAuthAccessToken() {
        `when`(oAuthService.accessToken(any())).thenReturn(fakeAccessToken)
    }

    private fun stubApiBalance() {
        `when`(apiService.balance(any()))
            .thenReturn(JSONObject(readFromFile("/balances.json", javaClass)))
    }

    private fun stubApiBalanceError() {
        `when`(apiService.balance(any()))
            .thenReturn(JSONObject("{\"error_code\": \"ITEM_LOGIN_REQUIRED \"}"))
    }
}