package com.bennorcombe.wyatt

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.work.ListenableWorker
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.data.OutgoingType
import com.bennorcombe.wyatt.utils.OUTGOINGS_TYPES_DATA_FILENAME
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import org.junit.After
import org.junit.Before
import org.mockito.Mockito.mock
import java.io.IOException

open class BaseTest {

    protected lateinit var dbInMemory: AppDatabase

    protected lateinit var context: Context

    protected val theme = R.style.AppTheme_NoActionBar

    protected var mockNavController: NavController = mock(NavController::class.java)

    @Before
    fun createDb() {
        context = ApplicationProvider.getApplicationContext<Context>()
        dbInMemory = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        context.assets.open(OUTGOINGS_TYPES_DATA_FILENAME).use { inputStream ->
            JsonReader(inputStream.reader()).use { jsonReader ->
                val outgoingsTypeType = object : TypeToken<List<OutgoingType>>() {}.type
                val outgoingTypeList: List<OutgoingType> =
                    Gson().fromJson(jsonReader, outgoingsTypeType)

                dbInMemory.outgoingsTypeDao().insertAll(outgoingTypeList)

                ListenableWorker.Result.success()
            }
        }
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        dbInMemory.close()
    }

    protected fun setFragmentNav(fragment: Fragment, nav: NavController): Fragment {
        return fragment.also {
            it.viewLifecycleOwnerLiveData.observeForever{ viewLifecycleOwner ->
                if (viewLifecycleOwner != null) {
                    Navigation.setViewNavController(fragment.requireView(), nav)
                }
            }
        }
    }
}