package com.bennorcombe.wyatt

import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.*
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bennorcombe.wyatt.data.currentYear
import com.bennorcombe.wyatt.data.months
import com.bennorcombe.wyatt.data.years
import com.bennorcombe.wyatt.ui.fragments.balances.BalanceHistoryFragment
import com.bennorcombe.wyatt.ui.fragments.balances.BalancesFragment
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.equalTo
import org.joda.time.DateTime
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@RunWith(AndroidJUnit4::class)
class BalanceHistoryTest: BaseTest() {

    @Before
    fun fragmentSetup() {
        launchFragmentInContainer(themeResId = theme, fragmentArgs = Bundle()) {
            setFragmentNav(BalanceHistoryFragment(), mockNavController)
        }
    }
    @Test
    fun testAddBalanceHistory() {
        onView(withId(R.id.month_spinner)).perform(click())
        val currentMonth = DateTime.now().toString("MM")
        onData(allOf(equalTo(months()[currentMonth.toInt() -1]))).perform(click())

        onView(withId(R.id.year_spinner)).perform(click())
        onData(allOf(equalTo(years().find { it == currentYear }))).perform(click())

        onView(withId(R.id.balance_history_amount)).perform(typeText("88000"))
        closeSoftKeyboard()

        onView(withId(R.id.fab_add_balance_history)).perform(click())

        launchFragmentInContainer<BalancesFragment>(themeResId = theme)

        onView(withId(R.id.balance_history_items))
            .check(matches(hasDescendant(withText("£ 88000.00"))))

        onView(withId(R.id.balance_history_items))
            .check(matches(hasDescendant(withText("$currentYear-$currentMonth-23"))))
    }
}