package com.bennorcombe.wyatt

import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bennorcombe.wyatt.actions.ScrollToBottomAction
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.data.OutgoingType
import com.bennorcombe.wyatt.ui.fragments.outgoings.OutgoingsFragment
import com.bennorcombe.wyatt.ui.fragments.outgoings.SaveOutgoingFragment
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SaveOutgoingTest: BaseTest() {

    @Before
    fun fragmentSetup() {
        launchFragmentInContainer(themeResId = theme, fragmentArgs = Bundle()) {
            setFragmentNav(SaveOutgoingFragment(), mockNavController)
        }
    }

    @Test
    fun testOutgoingSavedInDb() {
        val outgoingsType = AppDatabase.getInstance(context).outgoingsTypeDao().all()[1]
        val outgoingsName = "My Test Outgoing"
        val outgoingsAmount = "10.55"

        onView(withId(R.id.outgoings_type_spinner))
            .perform(click())
        onData(
            allOf(
                CoreMatchers.instanceOf(OutgoingType::class.java),
                equalTo(outgoingsType)
            )
        ).perform(click())

        onView(withId(R.id.outgoing_name))
            .perform(typeText(outgoingsName))
        onView(withId(R.id.outgoing_amount))
            .perform(typeText(outgoingsAmount))
        Espresso.closeSoftKeyboard()

        onView(withId(R.id.fab_add_outgoing)).perform(click())

        launchFragmentInContainer<OutgoingsFragment>(themeResId = theme, fragmentArgs = Bundle())

        onView(withId(R.id.yearly_outgoings_items))
            .perform(ScrollToBottomAction()).check(
                matches(
                    hasDescendant(withText("My Test Outgoing"))
                )
            )
    }

}