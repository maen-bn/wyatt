package com.bennorcombe.wyatt

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.DrawerActions
import androidx.test.espresso.contrib.NavigationViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bennorcombe.wyatt.ui.activities.MainActivity
import org.hamcrest.CoreMatchers.containsString
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class NavigationTest: BaseTest() {
    @Rule
    @JvmField
    val mainActivity = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun testNavigationDrawerChange() {
        onView(withId(R.id.current_month_overview_title)).check(matches(withText(containsString("Current Month Overview"))))
        onView(withId(R.id.current_year_overview_title)).check(matches(withText(containsString("Current Year Overview"))))

        navigateTo(R.id.nav_balances)
        onView(withId(R.id.text_current_balance_title)).check(matches(withText(containsString("Current balance"))))

        navigateTo(R.id.nav_outgoings)
        onView(withId(R.id.text_monthly_outgoings_title)).check(matches(withText(containsString("Outgoings"))))
    }

    private fun navigateTo(id: Int) {
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open())
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(id))
    }
}


