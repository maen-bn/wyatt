package com.bennorcombe.wyatt

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bennorcombe.wyatt.services.KeyStoreService
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class KeyStoreServiceTest: BaseTest() {

    private lateinit var service: KeyStoreService

    private val alias = "MyTest"

    private val toBeEncrypted = "MySecret"

    private lateinit var encrypted: String

    private lateinit var iv: String

    @Before
    fun initService() {
        service = KeyStoreService()
        encrypted = service.encrypt(alias, toBeEncrypted)
        iv = service.iv()
    }

    @Test
    fun testEncryptingStringWillAlsoProvideAnIV() {
        assertNotEquals(toBeEncrypted, encrypted)
        assert(iv.isNotEmpty())
    }

    @Test
    fun testDecryptingReturnsCorrectString() {
        val unEncrypted = service.decrypt(alias, encrypted, iv)
        assertNotNull(unEncrypted)
        assertEquals(toBeEncrypted, unEncrypted)
    }
}