package com.bennorcombe.wyatt

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class OutgoingTypesTest: BaseTest() {

    @Test
    fun testOutgoingTypesArePopulated() {
        val outgoingType= dbInMemory.outgoingsTypeDao().all()
        assertEquals(outgoingType.first().name, "Monthly outgoing")
        assertEquals(outgoingType.last().name, "Yearly outgoing")
    }

}