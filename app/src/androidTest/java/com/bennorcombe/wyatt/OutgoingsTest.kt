package com.bennorcombe.wyatt

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.data.Outgoing
import com.bennorcombe.wyatt.ui.fragments.outgoings.OutgoingsFragment
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.math.BigDecimal

@RunWith(AndroidJUnit4::class)
class OutgoingsTest: BaseTest() {

    @Before
    fun fragmentSetup() {
        val outgoing = Outgoing(
            id = 1, name = "My Test Outgoing", amount = BigDecimal(10), outgoingTypeId = 1
        )
        AppDatabase.getInstance(context).outgoingDao().save(outgoing)
        launchFragmentInContainer<OutgoingsFragment>(themeResId = theme)
    }

    @Test
    fun testOutgoingsFragment() {
        onView(withId(R.id.monthly_outgoings_items))
            .check(matches(hasDescendant(withText("My Test Outgoing"))))
    }
}