package com.bennorcombe.wyatt

import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bennorcombe.wyatt.actions.ScrollToBottomAction
import com.bennorcombe.wyatt.data.AppDatabase
import com.bennorcombe.wyatt.data.OutgoingType
import com.bennorcombe.wyatt.ui.fragments.banks.BanksFragment
import com.bennorcombe.wyatt.ui.fragments.banks.SaveBankFragment
import com.bennorcombe.wyatt.ui.fragments.outgoings.OutgoingsFragment
import org.hamcrest.CoreMatchers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SaveManualBankTest: BaseTest() {

    @Before
    fun fragmentSetup() {
        launchFragmentInContainer(themeResId = theme, fragmentArgs = Bundle()) {
            setFragmentNav(SaveBankFragment(), mockNavController)
        }
    }

    @Test
    fun testManualBankIsSavedToDb() {
        val bankName = "My Test Bank"
        val bankBalance = "5600"
        val estimatedMonthlyChange = "100"
        val changeDay = 9

        Espresso.onView(ViewMatchers.withId(R.id.bank_name_text))
            .perform(ViewActions.typeText(bankName))
        Espresso.onView(ViewMatchers.withId(R.id.bank_balance_text))
            .perform(ViewActions.typeText(bankBalance))
        Espresso.onView(ViewMatchers.withId(R.id.bank_estimated_change_text))
            .perform(ViewActions.typeText(estimatedMonthlyChange))
        Espresso.closeSoftKeyboard()

        Espresso.onView(ViewMatchers.withId(R.id.bank_change_day_spinner))
            .perform(ViewActions.click())
        Espresso.onData(CoreMatchers.allOf(CoreMatchers.equalTo(changeDay)))
            .perform(ViewActions.click())

        Espresso.onView(ViewMatchers.withId(R.id.fab_save_bank)).perform(ViewActions.click())

        launchFragmentInContainer<BanksFragment>(themeResId = theme, fragmentArgs = Bundle())

        Espresso.onView(ViewMatchers.withId(R.id.banks_items)).perform(ScrollToBottomAction()).check(
            ViewAssertions.matches(
                ViewMatchers.hasDescendant(ViewMatchers.withText(bankName))
            )
        )

    }
}