package com.bennorcombe.wyatt

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.work.ListenableWorker
import androidx.work.WorkerFactory
import androidx.work.WorkerParameters
import androidx.work.testing.TestWorkerBuilder
import com.bennorcombe.wyatt.data.Balance
import com.bennorcombe.wyatt.data.BalanceDao
import com.bennorcombe.wyatt.services.BalanceService
import com.bennorcombe.wyatt.services.BankService
import com.bennorcombe.wyatt.services.NotificationService
import com.bennorcombe.wyatt.workers.UpdateBalanceWorker
import org.joda.time.DateTime
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.*
import java.math.BigDecimal
import java.util.concurrent.Executor
import java.util.concurrent.Executors

@RunWith(AndroidJUnit4::class)
class UpdateBalanceWorkerTest {
    private lateinit var context: Context
    private lateinit var executor: Executor
    private lateinit var sharedPreferences: SharedPreferences
    private val balanceDao = mock(BalanceDao::class.java)
    private val bankService = mock(BankService::class.java)
    private val notificationService = mock(NotificationService::class.java)
    private lateinit var balance: Balance

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()
        executor = Executors.newSingleThreadExecutor()
        balance = Balance(amount = BigDecimal(0), date = DateTime())
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        setupStubs()
    }

    @Test
    fun testBalanceIsNotUpdatedWhenAutoUpdateIsTurnedOff() {
        val worker = TestWorkerBuilder<UpdateBalanceWorker>(
            context = context,
            executor = executor
        ).apply {
            setWorkerFactory(TestWorkerFactory())
        }.build()
        val originalBalanceAmount = balance.amount
        sharedPreferences.edit().putBoolean("auto_balance_preference", false).commit()
        worker.doWork()
        assertEquals(originalBalanceAmount, balance.amount)
    }

    private fun setupStubs() {
        `when`(balanceDao.currentNow()).thenReturn(balance)
        `when`(balanceDao.save(any())).then {
            balance = (it.arguments[0] as Balance)
            Unit
        }
        `when`(bankService.totalBankBalance()).thenReturn(BigDecimal(176))
    }

    private inner class TestWorkerFactory: WorkerFactory() {
        override fun createWorker(
            appContext: Context,
            workerClassName: String,
            workerParameters: WorkerParameters
        ): ListenableWorker? {
            return UpdateBalanceWorker(
                appContext,
                workerParameters,
                BalanceService(balanceDao, bankService, notificationService),
                sharedPreferences
            )
        }

    }

    private fun <T> any(): T = Mockito.any<T>()
}
