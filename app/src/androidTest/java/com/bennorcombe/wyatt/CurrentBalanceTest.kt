package com.bennorcombe.wyatt

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.clearText
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bennorcombe.wyatt.ui.fragments.balances.BalancesFragment
import com.bennorcombe.wyatt.ui.fragments.balances.CurrentBalanceFragment
import org.hamcrest.CoreMatchers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito

@RunWith(AndroidJUnit4::class)
class CurrentBalanceTest: BaseTest() {

    @Before
    fun fragmentSetup() {
        mockNavController = Mockito.mock(NavController::class.java)
        launchFragmentInContainer(themeResId = theme){
            setFragmentNav(CurrentBalanceFragment(), mockNavController)
        }
    }

    @Test
    fun testSettingCurrentBalance() {
        Espresso.onView(ViewMatchers.withId(R.id.edit_current_balance_text))
            .perform(clearText(), ViewActions.typeText("76000"))
        Espresso.closeSoftKeyboard()
        Espresso.onView(ViewMatchers.withId(R.id.fab_set_current_balance)).perform(ViewActions.click())

        launchFragmentInContainer<BalancesFragment>(themeResId = theme)

        Espresso.onView(ViewMatchers.withId(R.id.text_current_balance)).check(
            ViewAssertions.matches(
                ViewMatchers.withText(CoreMatchers.containsString("76000"))
            )
        )
    }
}