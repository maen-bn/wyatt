<div align="center">![Wyatt](app/src/main/res/mipmap-xxxhdpi/ic_launcher_round.png)</div>
<div align="center"><a href="https://gitlab.com/maen-bn/wyatt/commits/master"><img alt="pipeline status" src="https://gitlab.com/maen-bn/wyatt/badges/master/pipeline.svg"></a></div>

<div align="center"><h1>Wyatt - Personal Budget Monitor</h1></div>

Wyatt is an Android application which can be used to track your monthly and yearly budget. Due to the current nature of the setup (see bank integrations below), no builds can be provided currently for widespread use.

## Screenshots


[<img src="screenshots/navbar_screenshot.png" width=170>](screenshots/navbar_screenshot.png)
[<img src="screenshots/dashboard_screenshot.png" width=170>](screenshots/dashboard_screenshot.png)
[<img src="screenshots/balances_screenshot.png" width=170>](screenshots/balances_screenshot.png)
[<img src="screenshots/outgoings_screenshot.png" width=170>](screenshots/outgoings_screenshot.png)
[<img src="screenshots/select_bank_screenshot.png" width=170>](screenshots/select_bank_screenshot.png)

## Features

* Tracking of your overall personal balance against your budget
* Visualisation on how well you're keep to your budget
* Visualisation of the historical changes in your personal overall balance
* Automated balance tracking by linking your personal bank accounts balance to the app (Currently only available if you self build the app and have a [Plaid](https://plaid.com) account. [See bank integration](#bank-integration))
* Logging of yearly and monthly outgoings for better budget tracking

## Requirements

* Min SDK API version 23
* [Plaid](https://plaid.com) account ([See bank integration](#bank-integration))

## Permissions

* Biometrics

## Bank integration

Wyatt has a feature which allows you to integrate your bank accounts into the app to allow automatic tracking of your funds against your budget. This is done using the API provided by [Plaid](https://plaid.com).

Unfortunately I cannot provided any builds of this app which allows the bank integration to function using Plaid's API as their free tier is a developer limited API which is also limited to 100 items (banks) per app.

If you have your own Plaid account you could make your own build of this app but make sure there is a file in the root of this project called `apikey.properties` which is setup in the following way:

```
PLAID_API_URL="https://sandbox.plaid.com"
PLAID_API_PUBLIC_KEY="YOUR OWN PUBLIC KEY"
PLAID_API_CLIENT_ID="YOUR OWN CLIENT ID"
PLAID_API_SECRET="YOUR OWN API SECRET"
PLAID_ENVIRONMENT="SANDBOX"
```

## App Security
The application uses your devices's PIN code/biometrics for authentication. Screenshots and task manager previews are switched off by default but can be turned on via settings